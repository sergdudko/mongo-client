/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  PIDCONTROL
 *	Убъет процесс iRetailCloudClient, если он существует (PID файл), и запишет свой id в файл
 *  
 */

"use strict"

//подгружаемые библиотеки
var OS = require('os'),
	FS = require('fs'),
	PATH = require('path');

function pidcontrol(){
	let _servicename = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;
	return new Promise(function(resolve){
		(new Promise(function(res){
			//читаю PID-файл
			let _tmp = JSON.parse(FS.readFileSync(PATH.join(OS.tmpdir(), _servicename+'.pid')));
			res(_tmp.pid);
		})).then(function(res){
			//убиваю процесс
			return new Promise(function(_res, _rej){
				if(process.kill(res)){
					_res(true);
				} else {
					_rej(new Error('Неизвестная ошибка.'));
				}
			});
		}).catch(function(){
			//не интересует, т.к. не смог прочитать pid-файл или не смог убить процесс (логично что должны быть права!!!)
		}).finally(function(){
			//создаю PID-файл
			FS.writeFileSync(PATH.join(OS.tmpdir(), _servicename+'.pid'), JSON.stringify({pid:process.pid}), {flag: "w"});
			resolve(true);
		});
	});
}

module.exports = pidcontrol;