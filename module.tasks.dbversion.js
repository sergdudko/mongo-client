/**
 *	iRetailCloudClient
 *	(c) 2020 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE DBVERSION
 *	Обновление версии СУБД
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path'),
	FS = require('fs');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js')),
	SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js'));
	
function dbversion(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK DBVERSION START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		/* -> конфигурация */
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK DBVERSION DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let version = '0.0.0.0';
		let oneStepTASK = function(files, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
			if(iteration_bad === 0)
				step++;
			if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение
				MSSQL.req("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+task.version+"'").then(function(val_select){ 	//получаю версию
					if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
						if(typeof(val_select[0]['ParamValue'].value) === 'string'){
							version = val_select[0]['ParamValue'].value;
						}
					}
					let flg;
					for(const v in files){
						if(FUNCTIONS.comparevers(v, version) === 1){
							logger.debug("Необходимо обновить версию СУБД c v"+version+" до v"+v);
							flg = v;
							break;
						}
					}
					if(typeof(flg) === 'string'){
						FS.readFile(PATH.normalize(PATH.join(task.dir, files[flg])), (err, data) => {
							if (err){
								logger.debug("Ошибка чтения файла"+files[flg]+':'+err.message);
								oneStepTASK(files, ++iteration_bad);
							} else {
								MSSQL.connection.beginTransaction(function(err){
									if(err){
										logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не удалось начать транзакцию!');
										setTimeout(oneStepTASK, 10000, files, ++iteration_bad);
									} else {
										const errorHandler = function(files, _iteration_bad){
											MSSQL.connection.rollbackTransaction(function(_err){
												if(_err){
													logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу закрыть транзакцию: '+ _err);
												}
												MSSQL.connection.reset(function(_err2){	//сброс соединения (для защиты от всякой мути типа переключения соединения на другую БД и т.п.)
													if(_err2){
														logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу сбросить соединение: '+ _err2);
													}
													setTimeout(oneStepTASK, 10000, files, ++_iteration_bad);
												});
											});
										}
										const commitHandler = function(files, _iteration_bad){
											MSSQL.connection.commitTransaction(function(_err){
												if(_err){
													logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу зафиксировать транзакцию: '+ _err);
													errorHandler(files, _iteration_bad);
												} else {
													++step;
													MSSQL.connection.reset(function(_err2){
														if(_err2){
															logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу сбросить соединение: '+ _err2);
															errorHandler(files, _iteration_bad);
														} else {
															logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| выполнено обновление СУБД с v'+version+' до v'+flg+'.');
															SENDMAIL.send({
																message:LOGGER.datetime()+'Обновлена версия СУБД с v'+version+' до v'+flg+'.', 
																theme:'Обновлена версия СУБД с v'+version+' до v'+flg+'.'
															});
															oneStepTASK(files, 0);
														}
													});
												}
											});
										}
										MSSQL.multireq(data.toString('utf8')).then(function(){
											MSSQL.version(task.version, flg).then(function(){ 	//обновляю версию
												commitHandler(files, ++iteration_bad);
											}).catch(function(err){
												logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка обновления версии: '+ err); 
												errorHandler(files, ++iteration_bad);
											});
										}).catch(function(err){
											logger.debug("Ошибка выполнения скрипта "+files[flg]+":"+err);
											errorHandler(files, ++iteration_bad);
										});
									}
								});
							}
						});
					} else {
						exit(true);
					}
				}).catch(function(err){
					logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"Не могу прочитать версию для "+task.version+": "+err);
					exit();
				});
			} else {
				exit();
			}
		}
		FS.readdir(PATH.normalize(task.dir), function(err,filesall){
			try{
				if(err){
					throw err;
				} else {
					let files = {},
					_filesall = [];					
					for(let key = 0; key < filesall.length; key++){ 
						if((/^(\d+\.)(\d+\.)(\d+\.)(\d+).*\.sql$/gi).test(filesall[key])){	//выбираем только файлы .sql с версией
							_filesall.push(filesall[key]);
						}
					}
					_filesall = _filesall.sort(FUNCTIONS.comparevers);
					for(let key = 0; key < _filesall.length; key++){
						let v = _filesall[key].match(/^(\d+\.)(\d+\.)(\d+\.)(\d+)/gi);	//получаю версию файла в виде XX.XX.XX.XX, где XX - число
						if(Array.isArray(v) && (typeof(v[0]) === 'string')){
							if(typeof(files[v[0]]) === 'string'){
								throw new Error("Найдена коллизия в версиях скриптов! Имена файлов:"+_filesall[key]+', '+files[v[0]]);
							} else {
								files[v[0]] = _filesall[key];
							}
						}
					}
					oneStepTASK(files, 0);
				}
			} catch (err){
				logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"Не могу прочитать директорию с скриптами обновления: "+err);
				exit();
			}
		});
	});
}

module.exports = dbversion;