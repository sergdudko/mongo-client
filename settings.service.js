/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	SETTINGS
 *  SERVICE
 *	Настройки службы windows
 *
 */

"use strict"

//подгружаемые библиотеки
var OS = require('os'),
	PATH = require('path');
	
//подгружаемые модули		
var SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename,
	MYENV = require(PATH.join(__dirname, 'settings.parametrs.js')).myenv;

let svc;	
if(OS.platform() === 'win32'){
	let NW = require('node-windows');

	svc = new NW.Service({
		name:SERVICENAME,
		description: 'Служба для работы с iRetailCloudServer v3.',
		script: PATH.join(__dirname, "process.master.js"),
		wait: 30,
		grow: 0,
		maxRetries: 99999999,
		maxRestarts: 99999999,
		env: [{
			name: "NODE_ENV",
			value: MYENV
		},
		{
			name: "HOME",
			value: PATH.join(__dirname)
		}]
	});
} else if(OS.platform() === 'linux') {
	let NW = require('node-linux');
	svc = new NW.Service({
		name:SERVICENAME,
		description: 'Служба для работы с iRetailCloudServer v3.',
		script: PATH.join(__dirname, "process.master.js"),
		user: "root",
		group: "root",
		wait: 30,
		grow: 0,
		env: [{
			name: "NODE_ENV",
			value: MYENV
		},
		{
			name: "HOME",
			value: PATH.join(__dirname)
		}]
	});
} else {
	throw new Error('SERVICE-> Неподдерживаемая платформа!');
}
	
module.exports = svc;