/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE SENDDATA
 *	Отправка документов из T_PostMessages на сервер
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));
	
function senddata(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK SENDDATA START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = ''+PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const link = CONFIG.server.url+'/v3/data'; //ссылка отправки данных
		/* -> конфигурация */
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK SENDDATA DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let auth;
		MSSQL.getconfig().then(function(val){
			try{
				if(val && val.login && val.password){
					auth = val.login + ':' + val.password;
				}
				MSSQL.req("SELECT TOP 100 * FROM [dbo].[T_PostMessages] WHERE [State] < 100 ORDER BY [State], [Id]").then(function(val_select){ 	//в state счетчик ошибок
					if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
						let oneStepTASK = function(iteration, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
							if(typeof(val_select[iteration]) === 'undefined'){
								res(_uid);
								logger.debug('TASK SENDDATA DONE');
							} else {
								if(iteration_bad === 0)
									step++;
								if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение.
									let _restsettings = {
										url:link,
										method:"POST",
										data:val_select[iteration].Body.value,
										datatype:"string",
										timeout: 300000,
										headers: {
											encoding: "gzip"
										}
									};
									if(typeof(auth) === 'string')
										_restsettings.auth = auth;
									FUNCTIONS.rest(_restsettings).then(function(req_data){	//выполняю POST запрос на сервер
										if(parseInt(parseInt(req_data[0])/100) === 2){
											let _str;
											try {
												if(typeof(req_data[1]) === 'object'){
													_str = JSON.stringify(req_data[1]);
												} else {
													_str = req_data[1];
												}
											} catch(_e){
												logger.warn(_e);
											}
											logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
										}else{
											let _str;
											try {
												if(typeof(req_data[1]) === 'object'){
													_str = JSON.stringify(req_data[1]);
												} else {
													_str = req_data[1];
												}
											} catch(_e){
												logger.warn(_e);
											}
											logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
										}
										let next_req;
										if((typeof(req_data) === 'object') && (typeof(req_data[1]) === 'object') && (req_data[1].status === 'ok')){
											next_req = "DELETE FROM [dbo].[T_PostMessages] WHERE [Id] = '"+val_select[iteration].Id.value+"'";
										} else {
											switch(typeof(req_data[1])){
												case 'object':
													(function(){
														let _answr = JSON.stringify(req_data[1]);
														next_req = "UPDATE [dbo].[T_PostMessages] SET [Error] = '"+_answr+"', [State] = '"+(parseInt(val_select[iteration].State.value, 10) + 1)+"' WHERE [Id] = '"+val_select[iteration].Id.value+"'";
													})();
													break;
												case 'undefined':
													(function(){
														next_req = "UPDATE [dbo].[T_PostMessages] SET [State] = '"+(parseInt(val_select[iteration].State.value, 10) + 1)+"' WHERE [Id] = '"+val_select[iteration].Id.value+"'";
													})();
													break;
												default:
													(function(){
														let _answr = ''+req_data[1];
														next_req = "UPDATE [dbo].[T_PostMessages] SET [Error] = '"+_answr+"', [State] = '"+(parseInt(val_select[iteration].State.value, 10) + 1)+"' WHERE [Id] = '"+val_select[iteration].Id.value+"'";
													})();
													break;
											}
										}
										MSSQL.req(next_req).catch(function(err){
											logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| MSSQL-> Ошибка удаления из T_PostMessages: "+err);
										}).finally(function(){
											if((typeof(req_data) === 'object') && (typeof(req_data[1]) === 'object') && (req_data[1].status === 'ok')){
												setTimeout(oneStepTASK, FUNCTIONS.randomgen(1000), ++iteration, 0);
											} else {
												setTimeout(oneStepTASK, FUNCTIONS.randomgen(1000), ++iteration, 0);	//переключаю на следующую позицию (позицию с ошибкой обработаю в последнюю очередь на следующем запуске)
											}
										});
									}).catch(function(error){	//ошибка в функции POST запроса
										logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+error);
										setTimeout(oneStepTASK, 60000, iteration, ++iteration_bad);
									}); 
								} else {
									exit();
								}
							}
						}
						setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), 0, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
					} else {
						res(_uid);
						logger.debug('TASK SENDDATA DONE');
					}
				}).catch(function(err){
					rej("Не могу прочитать таблицу T_PostMessages: "+err);
					logger.debug('TASK SENDDATA DONE');
				});
			} catch(err){	//ошибка глобальной обертки функции
				rej(err);
				logger.debug('TASK SENDDATA DONE');
			}
		}).catch(function(err){
			logger.warn(err);
			exit();
		});
	});
}

module.exports = senddata;