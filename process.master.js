/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	PROCESS
 *  START (MASTER)
 *	Мастер процесс, занимается контролем состояния соркеров task-серверов и саб-воркеров
 *	Также мастер убивает воркеров с зависшими заданиями и распределяет задания воркерам
 *
 */
 
 "use strict"
 
var PATH = require('path');

if(parseInt(process.version.split(".")[0].substr(1,2), 10) < 10){
	Promise = require("bluebird");
}

var PIDCONTROL = require(PATH.join(__dirname, 'pidcontrol.js'));

PIDCONTROL().then(function(){

	//ловим эксепшны
	var BUGTRACKER = require(PATH.join(__dirname, 'module.bugtracker.js'));	
	BUGTRACKER();
	
	//запускаю контроль RAM
	var MEMCTRL = require(PATH.join(__dirname, 'module.memctrl.js'));	
	MEMCTRL();
	
	//подгружаемые библиотеки
	var CLUSTER = require('cluster'),
		CONFIG = require('config'),
		FS = require('fs'),
		LODASH = require('lodash');
		
	//подгружаемые модули
	var SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
		LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
		PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
		FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
		SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;
		
	PROCSTORE.stderr = LOGGER.warn;
	
	var backupconf = {
		path:PATH.join(__dirname, SERVICENAME+'.dmp'), 
		key:"santaclaus", 
		timeout:30
	};
	PROCSTORE.backup(backupconf).catch(function(){
		return new Promise(function(res, rej){
			FS.promises.unlink(backupconf.path).catch(function(){
			}).finally(function(){
				return PROCSTORE.backup(backupconf);
			}).then(function(){
				res(true);
			}).catch(function(err){
				rej(err);
			});
		});
	}).catch(function(err){
		LOGGER.error(err);
	}).finally(function(){

		PROCSTORE.createServer({path:PATH.join(__dirname, SERVICENAME+".sock"), logins:{"iretail":"santaclaus"}});

		//глобальные переменные
		let VERSION = require(PATH.join(__dirname, 'package.json')).version,
		onUpdate = false;

		//создаю класс, который самостоятельно отслеживает состояние воркеров
		let IRetailCloudClient = function(proc, count){
			let self = this;
			
			if(typeof(count) !== 'number'){
				count = 1;
			}
			
			switch(proc){
				case 'task':
					self.path = PATH.join(__dirname, 'process.worker.task.js');
					self.name = "Task-worker";
					break;
				case 'sub':
					self.path = PATH.join(__dirname, 'process.worker.sub.js');
					self.name = "Sub-worker";
					break;
				default:
					throw new Error('Не задан тип процесса');
			}
			
			self.exitHandler = function(exit, signal){
				CLUSTER.setupMaster({
					exec: self.path,
					silent: true
				});
				let worker = CLUSTER.fork();
				self.listener(CLUSTER.workers[worker.id]);
				if(signal){
					PROCSTORE.dispatch({type:"CLEAR_TASK"});
					LOGGER.warn(self.name+' умер и был пересоздан!');
				}
			}
			
			self.listener = function(worker){
				CLUSTER.workers[worker.id].on('exit', self.exitHandler);
			}
			
			for(let i = 0; i < count; i++){
				const _i = i;
				setTimeout(self.exitHandler, _i*5000);
			}	
		};

		//убийца зависших воркеров
		function killerWorker(){
			try{
				for(const uid in PROCSTORE.getState().run){
					let _runtask = PROCSTORE.getState().run[uid];
					if(typeof(_runtask) === 'object'){
						if(_runtask.timestamp < (Date.now() - 120*60*1000)){	//убиваем воркер, если он выполняет задание дольше 2 часов
							if(typeof(CLUSTER.workers[_runtask.worker]) !== 'undefined'){
								CLUSTER.workers[_runtask.worker].process.kill();
							} else {
								PROCSTORE.dispatch({type:"CLEAR_TASK"});
							}
						}
					} else {
						PROCSTORE.dispatch({type:"CLEAR_TASK"});
					}
				}
			} catch(e){
				LOGGER.error('KILLERWORKER -> '+e);
			}
		}

		//функция прохода по заданиям
		function goTasks(){
			try{
				const _t0 = FUNCTIONS.hasher('ICCUpdater.json');
				if(
					(PROCSTORE.getState().update === VERSION) || //версия соответствует текущей
					((typeof(PROCSTORE.getState().tasks[_t0]) !== 'undefined') && (PROCSTORE.getState().logs[_t0] <  PROCSTORE.getState().updatetime) && (typeof(PROCSTORE.getState().run[_t0]) !== 'object')) //задача существует и еще не запускалась
				){	//проход по таскам
					for(let i = 0; i < PROCSTORE.getState().query.length; i++){	//проходим по очереди
						let key = PROCSTORE.getState().query[i];
						if((PROCSTORE.getState().update === VERSION) || (PROCSTORE.getState().query[i] === _t0)) {
							if((typeof(PROCSTORE.getState().logs[key]) !== 'number') || (((PROCSTORE.getState().tasks[key].interval * 60 *1000) + PROCSTORE.getState().logs[key]) < Date.now()) || (PROCSTORE.getState().update !== VERSION)){	//проверяем что таймаут истек
								let _nosilent = true;
								let _silent = PROCSTORE.getState().tasks[key].silent;
								if(Array.isArray(_silent)){
									let _d = Date.now();
									for(let _i = 0; _i < _silent.length; _i++){
										if((_d >= _silent[_i].start) && (_d <= _silent[_i].end)){
											_nosilent = false;
										}
									}
								}
								if((typeof(PROCSTORE.getState().run[key]) === 'undefined') && (_nosilent)){	//проверяем что таск не запущен и у него не "время тишины"
									for(const id in PROCSTORE.getState().worker){	//проходим по воркерам 
										if((PROCSTORE.getState().worker[id] === 'idle') && ((PROCSTORE.getState().status.mssql[id] === true) || (CONFIG.usedb === false)) && (typeof(CLUSTER.workers[id]) !== 'undefined')){	//проверяем что воркер свободен и соединен с БД
											try{
												CLUSTER.workers[id].send({cmd : "TASK_TO_WORKER", uid : key});
												PROCSTORE.dispatch({type:'RUN_TASK', payload: {uid:key, worker:id}});	//выставляю воркеру статус, что он занят, а задаче что она запущена (IPC может тормозить, поэтому в мастере)
											} catch(err){
												LOGGER.error('GOTASK -> Ошибка отправки задачи воркеру: ' + err);
											}
											break; //нужно выкинуть из цикла, чтобы другой свободный воркер не поймал дубликат задания
										}
									}
								}
							}
						}
					}
				} else if(!onUpdate) { //если установлен флаг обновления и задача обновления завершена, то ожидаю завершения всех задач и завершаю процесс
					let restarter = function(){
						if(LODASH.isEqual(PROCSTORE.getState().run, {})){
							LOGGER.warn('UPDATER-> Выполняю остановку процесса для обновления с '+VERSION+' по '+PROCSTORE.getState().update);
							setTimeout(process.exit, 40000, 0);	//бэкап раз в 30 сек, смещаю на 10
						} else {
							setTimeout(restarter, 15000);
						}
					}
					setTimeout(restarter, 10000);
					onUpdate = true;
				}
			} catch(err){
				LOGGER.error("GOTASK ->" + err);
			}
		}
		
		//устанавливаю версию приложения и очищаю запущенные задания (в случае дампа памяти актуально)
		PROCSTORE.dispatch({type:'SET_VERSION', payload: VERSION});
		
		//проверяем целостность папок
		FUNCTIONS.prestart([ 'config', 'mail', 'node_modules', 'tasks', 'logs' ]).then(()=>{
			
			//мастер активирован
			LOGGER.log('iRetail Cloud Client v'+VERSION+' started.');
			//запуск вспомогательных процессов (кол-во)
			new IRetailCloudClient("sub");
			//откладываю запуск воркеров, чтобы переменные и таски могли обновиться
			setTimeout(function(){
				//запуск процессов для выполнения тасков (кол-во)
				new IRetailCloudClient("task", CONFIG.workers);
			}, 10000);
			
			//извещаю по почте о запуске приложения (пока выключил, бесит при перезапуске)
			SENDMAIL.send({message:LOGGER.datetime()+'iRetail Cloud Client v'+VERSION+' started.', theme:'iRetail Cloud Client v'+VERSION+' started.'});
			
			//запускаю убийцу зависших воркеров
			setInterval(killerWorker, 15000);
			
			//запускаю распределение заданий
			setInterval(goTasks, 5000);
			
		}).catch(rej=>{LOGGER.error(rej);});
		
	});
	
});