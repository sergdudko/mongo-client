/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE UPDATE
 *	Обновление документов в бд
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));
	
function update(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK UPDATE START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		const link = CONFIG.server.url+'/v3/query'; //ссылка получения данных
		/* -> конфигурация */
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK UPDATE DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let auth;
		MSSQL.getconfig().then(function(val){
			try{
				if(val && val.login && val.password){
					auth = val.login + ':' + val.password;
				}
				let oneStepTASK = function(request, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
					if(iteration_bad === 0)
						step++;
					if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение (на 5 проходах были проблемы из-за статуса 206 - выполнялся долше 5 минут, увеличение же таймаута запроса затягивает выполнение)
						let _request = LODASH.clone(request);
						MSSQL.req("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+task.version+"'").then(function(val_select){ 	//получаю версию
							if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
								if(typeof(_request.find["version"]) !== 'object'){
									_request.find["version"] = {};
								}
								if(typeof(val_select[0]['ParamValue'].value) === 'string')
									_request.find["version"]["$gt"] = val_select[0]['ParamValue'].value;
							}
							let headers = {"ics-cache-timeout":task.cache};	//задаю хидеры
							let _restsettings = {
								url:link,
								method:"POST",
								data:_request,
								datatype:"object",
								headers:headers
							};
							if(typeof(auth) === 'string')
								_restsettings.auth = auth;
							FUNCTIONS.rest(_restsettings).then(function(req_data){	//выполняю POST запрос на сервер (упрощенный вариант, без 208 статуса и хидеров запроса)
								if(parseInt(parseInt(req_data[0])/100) === 2){
									if((req_data[0] === 200) || (req_data[0] === 201)){
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]);
									} else {
										let _str;
										try {
											if(typeof(req_data[1]) === 'object'){
												_str = JSON.stringify(req_data[1]);
											} else {
												_str = req_data[1];
											}
										} catch(_e){
											logger.warn(_e);
										}
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
									}
								}else{
									let _str;
									try {
										if(typeof(req_data[1]) === 'object'){
											_str = JSON.stringify(req_data[1]);
										} else {
											_str = req_data[1];
										}
									} catch(_e){
										logger.warn(_e);
									}
									logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
								}
								switch(req_data[0]){
									case 200:
									case 201:
										(function(){
											let oneUpdate = function(_i){
												if(_i < req_data[1].length){
													let _lineindex = 0;	
													let _linearr = FUNCTIONS.tdata({
														data: [req_data[1][_i]], 
														table: task.table
													});
													let _version = req_data[1][_i].version;
													if(typeof(_version) === 'undefined'){
														logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Документ '+req_data[1][_i]["_id"]+' не содержит версии!');
														setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
													}else if(typeof(_linearr) === 'object'){
														let oneLineUpdate = function(){
															let _line = _linearr[_lineindex];
															if(typeof(_line) === 'object'){
																for(const key in _line){	//удаляю undefined поля
																	if(typeof(_line[key]) === 'undefined')
																		_line[key] = "";
																}
																let select_req = "SELECT TOP 1 * FROM "+task.table.name;
																let where_req = " WHERE ";
																let _tline = Object.keys(_line);
																for(let col=0; col < task.table.matching.length; col++){
																	switch(col){
																		case (task.table.matching.length-1):
																			where_req = where_req+"["+task.table.matching[col]+"] = '"+_line[task.table.matching[col]]+"'";
																			break;
																		default:
																			where_req = where_req+"["+task.table.matching[col]+"] = '"+_line[task.table.matching[col]]+"' AND ";
																			break;
																	}
																	let _tindex = _tline.indexOf(task.table.matching[col]);	//удаляю matching поля из update
																	if(_tindex !== -1)
																		_tline.splice(_tindex, 1);
																}
																select_req = select_req+where_req;
																MSSQL.req(select_req).then(function(val_select2){	//проверяю наличие дока в бд
																	let next_req;
																	if((typeof(val_select2) === 'object') && (!LODASH.isEqual(val_select2, []))){
																		next_req = "UPDATE "+task.table.name+" SET ";
																		for(let _j = 0; _j < _tline.length; _j++){
																			switch(_j){
																				case(_tline.length-1):
																					next_req = next_req+ "["+_tline[_j]+"] = '"+_line[_tline[_j]]+"'";
																					break;
																				default:
																					next_req = next_req+ "["+_tline[_j]+"] = '"+_line[_tline[_j]]+"', ";
																					break;
																			}
																		}
																		next_req = next_req+where_req;
																		MSSQL.req(next_req).then(function(){	//обновляю данные в таблице
																			return MSSQL.version(task.version, _version).catch(function(err){ logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка обновления версии: '+ err); });	//обновляю версию
																		}).then(function(){
																			_lineindex++;
																			oneLineUpdate();	//следующая строка таблицы
																		}).catch(function(err){
																			logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| MSSQL-> Ошибка запроса: '+err);
																			setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
																		});
																	} else {
																		MSSQL.version(task.version, _version).catch(function(err){ 	//обновляю версию даже, если документа нет
																			logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка обновления версии: '+ err); 
																		}).finally(function(){
																			_lineindex++;
																			oneLineUpdate();	//следующая строка таблицы
																		});
																	}
																	
																}).catch(function(err){
																	logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| MSSQL-> Ошибка запроса: '+err);
																	setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
																});
															} else {
																_i++;
																oneUpdate(_i);	//следующий документ запроса
															}
														}
														oneLineUpdate();
													} else {
														if(typeof(_linearr) === 'string') { logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| TRANSFORM -> Ошибка трансформации данных: '+_linearr); }
														setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
													}
												} else {
													setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), _request, 0); 
												}
											}
											oneUpdate(0);
										})();
										break;
									case 204:	//данных нет, задача завершена
										(function(){
											exit(true);
										})();
										break;
									case 206:	//данных нет
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); //запустим через 1 минуту, но добавим флаг не успешного выполнения чтобы исключить вероятность зависания
										})();
										break;
									default:
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
										})();
										break;
								}
							}).catch(function(error){	//ошибка в функции POST запроса
								logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+error);
								setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
							});
						}).catch(function(err){
							rej("Не могу прочитать версию для "+task.version+": "+err);
							logger.debug('TASK UPDATE DONE');
						});
					} else {
						exit();
					}
				}
				const req = ({"find": task.request, "limit": task.limit, "skip":0, "sort":{"version": 1}, "requires": []});	//первый запуск сортировка по версии (это важно)
				setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), req, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
			} catch(err){	//ошибка глобальной обертки функции
				rej(err);
				logger.debug('TASK UPDATE DONE');
			}
		}).catch(function(err){
			logger.warn(err);
			exit();
		});
	});
}

module.exports = update;