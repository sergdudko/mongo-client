/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  MSSQL
 *	Работа с БД MSSQL	
 *  
 */

"use strict"

//подгружаемые библиотеки
var TEDIOUS = require('tedious'),
	LODASH = require('lodash'),
	CONFIG = require('config'),
	CLUSTER = require('cluster'),
	PATH = require('path');

//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js'));
	
var mainmodule = {};

var connected = false;

var errDate = 0;
var errLog = [];

//отправка ошибки при долгом расконнекте с БД
function connError(errtext){
	if(errLog.length === 0){
		if(errDate === 0) { errDate = Date.now(); }
		errLog.push(errtext); 
		setTimeout(function(){
			if(connected !== true){
				let errText = "";
				for(let i = 0; i < errLog.length; i++){
					errText = errText + errLog[i] + '\r\n';
				}
				LOGGER.error('База данных недоступна в течение '+parseInt((Date.now() - errDate)/60000)+'min, Ошибки:\r\n'+errText);
			} else {
				errDate = 0;
			}
			errLog = [];
		}, 5*60*1000);
	} else {
		if(errLog.length < 10) { 
			errLog.push(errtext); 
		} else {
			errLog = errLog.splice(1);
			errLog.push(errtext);
		}
	}
}
	
//функция соединения с mssql
function connMSSQL(callback = function(){return;}){
	let config = {
		userName: CONFIG.db.user.toString(),
		password: CONFIG.db.password.toString(),
		server: CONFIG.db.server.toString(),
		options:{
			port: 1433,
			database: CONFIG.db.database.toString(),
			fallbackToDefaultDb: false,
			enableAnsiNullDefault: true,
			connectTimeout: 30000,	//30 сек
			requestTimeout: 60000,	//60 сек запрос
			cancelTimeout: 5000,
			packetSize: 4096,
			useUTC: false,
			encrypt: false,
			dateFormat: "mdy",
			enableAnsiNull: true,
			enableAnsiPadding: true,
			enableAnsiWarnings: true,
			enableConcatNullYieldsNull: true,
			enableCursorCloseOnCommit: false,
			enableImplicitTransactions: false,
			enableNumericRoundabort: false,
			enableQuotedIdentifier: true,
			useColumnNames: true,	//используем имена столбцов в запросах
			rowCollectionOnRequestCompletion: true,
			appName: "iRetailCloudClient"
		}
	};
	mainmodule.connection = new TEDIOUS.Connection(config);
	mainmodule.connection.on('connect', function(err) {
		if(err){
			if(!CLUSTER.isMaster)
				PROCSTORE.dispatch({type:'STATUS_MSSQL', payload: {worker:CLUSTER.worker.id, status:false}});
			connected = false;
			LOGGER.warn('MSSQL-> Ошибка соединения: ' + err);
			connError('MSSQL-> Ошибка соединения: ' + err);
		} else {
			LOGGER.log('MSSQL-> Соединение установлено!');
			if(!CLUSTER.isMaster)
				PROCSTORE.dispatch({type:'STATUS_MSSQL', payload: {worker:CLUSTER.worker.id, status:true}});
			connected = true;
			callback();
		}
	});
	mainmodule.connection.on('error', function(err) {
		LOGGER.warn('MSSQL-> Ошибка соединения: ' + err);
		connError('MSSQL-> Ошибка соединения: ' + err);
	});
	mainmodule.connection.on('end', function() { 
		LOGGER.log('MSSQL-> Соединение закрыто!');
		connected = false;
		if(!CLUSTER.isMaster){
			PROCSTORE.dispatch({type:'STATUS_MSSQL', payload: {worker:CLUSTER.worker.id, status:false}});
			setTimeout(connMSSQL, 1000+((CLUSTER.worker.id%10)*100), callback);
		} else {
			setTimeout(connMSSQL, 1000, callback);
		}
	});
}

//ожидание соединения с mssql
function connMSSQLFirst(){
	return new Promise(function(resolve){
		let flag = true;
		function c(){
			connMSSQL(function(){
				if(flag && connected){
					flag = false;
					resolve();
				}
			});
		}
		c();
	});
}

//запись данных (bulk) в mssql
function bulkMSSQL(_task, _data, _summCount){
	LOGGER.debug('MSSQL BULKMSSQL START');
	return new Promise(function(resolve, reject){
		let _timeout = 600000; //таймаут 10 мин
		const task = LODASH.clone(_task); //чистим объекты, кроме _summCount
		const data = LODASH.clone(_data);
		if(data.length > 0){
			if(connected){ //проверяю статус соединения
				try {
					const columns = LODASH.keys(data[0]);
					let options = { keepNulls: true };
					let bulkLoad = mainmodule.connection.newBulkLoad(task.table.name, options, function (error, rowCount) {
						if(error){
							reject(error);
							LOGGER.debug('MSSQL BULKMSSQL DONE');
						} else {
							_summCount.summ = _summCount.summ + rowCount;
							resolve('Добавлено строк: ' + _summCount.summ);
							LOGGER.debug('MSSQL BULKMSSQL DONE');
						}
					});
					for(const col in columns){
						if(task.table.notnull.indexOf(columns[col]) === -1){
							bulkLoad.addColumn(columns[col], TEDIOUS.TYPES.NVarChar, { length: 255, nullable: true });
						}else {
							bulkLoad.addColumn(columns[col], TEDIOUS.TYPES.NVarChar, { length: 255, nullable: false });
						}
					}
					for(let key = 0; key < data.length; key++){
						bulkLoad.addRow(data[key]);
					}
					bulkLoad.setTimeout(_timeout);
					mainmodule.connection.execBulkLoad(bulkLoad);
				} catch(err){
					reject(err);
					LOGGER.debug('MSSQL BULKMSSQL DONE');
				}
			} else {
				reject(new Error('Соединение с БД отсутствует!'));
				LOGGER.debug('MSSQL BULKMSSQL DONE');
			}
		} else { //для нулевой даты возвращаем ок
			resolve('ok');
			LOGGER.debug('MSSQL BULKMSSQL DONE');
		}
	});
}

//функция работы с запросами в mssql
/*	пример запроса в SQL
reqMSSQL('SELECT TOP 1 [Key],[Идентификатор],[Название],[CostPrice],[Supply_code] FROM [aptekajet].[dbo].[Остатки]').then(function(val){
	//результат запроса в массиве val, результаты: val[строка][столбец].value
}).catch(function(err){
	//ошибка обработки
}); 
*/
function reqMSSQL(db_request){
	LOGGER.debug(db_request);
	return new Promise(function(resolve, reject){
		let _timeout = 60000; //таймаут 60 сек
		if(connected){ //проверяю статус соединения
			let _reqcallback = function(err, rowCount, rows) {
				request = null;
				if(err){
					LOGGER.debug(db_request+' | DONE');
					reject(err);
				} else {
					LOGGER.debug(db_request+' | DONE');
					resolve(rows);
				}
			/*	mainmodule.connection.reset(function(err){	//сброс соединения грохнет транзакцию, поэтому не использую
					if(err){
						reject(err);
					} else {
						resolve(rows);
					}
				});*/
			}
			let request = new TEDIOUS.Request(db_request, _reqcallback);
			request.setTimeout(_timeout);
			mainmodule.connection.execSql(request);
		}else{
			LOGGER.debug(db_request+' | DONE');
			reject(new Error('Соединение с БД отсутствует!'));
		}
	});
}

//функция работы с очередью запросов в mssql (разделитель GO)
function multiReqMSSQL(db_request){
	return new Promise(function(resolve, reject){
		let req_array = db_request.split(/(^|\s){1}GO(\s|$){1}/gi).map(function(arg){ if(/^\s*$/gi.test(arg) === false){ return arg; }}).filter(function(arg){ return typeof(arg) === 'string';});
		LOGGER.warn('Будет выполнено запросов: '+req_array.length);
		function recursiveRequest(array, i, lastmessage){
			LOGGER.debug('Шаг : '+i+"/"+array.length);
			if(i === array.length)
				return resolve(lastmessage);
			reqMSSQL(array[i]).then(function(message){
				setTimeout(recursiveRequest, 10, array, ++i, message);
			}).catch(function(err){
				return reject(err);
			});
		}
		recursiveRequest(req_array, 0, "Не найдено запросов для обработки!");
	});
}

//функция проверки наличия служебных таблиц (чистая)
function testWorkTable(){
	LOGGER.debug('MSSQL TESTWORKTABLE START');
	//служебные таблицы
	let table_hash = 'CREATE TABLE [dbo].[ICS_Hash]([Hash] [nvarchar](50) NOT NULL,[HashResult] [nvarchar](50) NOT NULL,[TaskName] [nvarchar](50) NOT NULL,[TimeStamp] [datetime] NULL CONSTRAINT [DF_ICS_Hash_TimeStamp]  DEFAULT (getdate()), [LastUid] [varchar](50) NULL) ON [PRIMARY]';
	let table_task = 'CREATE TABLE [dbo].[ICS_Task]([TaskName] [nvarchar](100) NOT NULL,[Hash] [nvarchar](50) NOT NULL,[TimeStamp] [datetime] NULL CONSTRAINT [PK_ICS_TaskName_TimeStamp]  DEFAULT (getdate()), [Epoch] [bigint] NULL, CONSTRAINT [PK_ICS_TaskName] PRIMARY KEY CLUSTERED ([TaskName] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]';
	let table_hash_tg = 'CREATE TRIGGER [dbo].[ICS_Hash_TimeStamp] ON  [dbo].[ICS_Hash] AFTER UPDATE AS BEGIN SET NOCOUNT ON;UPDATE [dbo].[ICS_Hash] SET [TimeStamp] = GETDATE() FROM [ICS_Hash] INNER JOIN inserted ON inserted.TaskName = [ICS_Hash].TaskName AND inserted.Hash = [ICS_Hash].Hash END';
	let table_task_tg = 'CREATE TRIGGER [dbo].[ICS_Task_TimeStamp] ON  [dbo].[ICS_Task] AFTER UPDATE AS BEGIN SET NOCOUNT ON;UPDATE [dbo].[ICS_Task] SET [TimeStamp] = GETDATE() FROM [ICS_Task] INNER JOIN inserted ON inserted.TaskName = [ICS_Task].TaskName END';
	let table_hash_index = 'CREATE UNIQUE CLUSTERED INDEX ICS_Hash_ics_hash ON dbo.ICS_Hash(Hash, TaskName)';
	return new Promise(function(res, rej){	//поочередно делаем запросы в sql (одно соединение на процесс)
		(new Promise(function(resolve, reject){	//проверка таблицы хэшей и её создание, в случае отсутствия
			reqMSSQL('SELECT TOP 1 * FROM [dbo].[ICS_Hash]').then(function(){
				resolve('ok');
			}).catch(function(){
				reqMSSQL(table_hash).then(function(){
					LOGGER.log('Создана служебная таблица [dbo].[ICS_Hash]!');
					reqMSSQL(table_hash_tg).catch(function(err3){
						LOGGER.error('Не создан триггер в таблице [dbo].[ICS_Hash]: ' + err3);
					}).finally(function(){
						reqMSSQL(table_hash_index).catch(function(err4){
							LOGGER.error('Не создан индекс в таблице [dbo].[ICS_Hash]: ' + err4);
						}).finally(function(){
							resolve('ok');
						});
					});
				}).catch(function(err2){
					reject('Не создана таблица [dbo].[ICS_Hash]: '+err2);
				});
			});
		})).then(function(){
			return new Promise(function(resolve, reject){	//проверка таблицы задач и её создание, в случае отсутствия
				reqMSSQL('SELECT TOP 1 * FROM [dbo].[ICS_Task]').then(function(){
					resolve('ok');
				}).catch(function(){
					reqMSSQL(table_task).then(function(){
						reqMSSQL(table_task_tg).then(function(){
							LOGGER.log('Создана служебная таблица [dbo].[ICS_Task]!');
							resolve('ok');
						}).catch(function(){
							LOGGER.error('Не создан триггер в таблице [dbo].[ICS_Task]!');
							resolve('ok');
						});
					}).catch(function(err2){
						reject('Не создана таблица [dbo].[ICS_Task]: '+err2);
					});
				});
			});
		}).then(function(_val){
			res(_val);
			LOGGER.debug('MSSQL TESTWORKTABLE DONE');
		}).catch(function(_err){
			rej(_err);
			LOGGER.debug('MSSQL TESTWORKTABLE DONE');
		});
	});
}

//функция обновления таблицы [ICS_Task]
function icsTasks(_uid, flag){
	LOGGER.debug('MSSQL ICSTASK START');
	return new Promise(function(res, rej){
		let select_task = "SELECT TOP 1 [TaskName],[Hash] FROM [dbo].[ICS_Task] WHERE [TaskName] = '"+PROCSTORE.getState().names[_uid]+"'";
		let taskHash = LODASH.clone(PROCSTORE.getState().tasks[_uid]);
		//удаление и добавление полей похерит хэши задач и вызовет перезаливку всех синхронизированных таблиц
		delete taskHash.interval;	//интервал запуска не меняет суть задачи
		delete taskHash.cache;	//таймаут кэширования не меняет суть задачи
		delete taskHash.silent;	//время тишины (таск не будет запущен в это время) не меняет суть задачи
		delete taskHash.description;	//описание задачи, дополнительные данные (в коде не участвуют)
		delete taskHash.author;	//автор задачи, дополнительные данные (в коде не участвуют)
		let _taskSHA = FUNCTIONS.hasher(JSON.stringify(taskHash));
		reqMSSQL(select_task).then(function(val){
			if((flag === 'run') && (val[0]['Hash'].value !== _taskSHA)){
				let update_task = "UPDATE [dbo].[ICS_Task] SET [Hash] = '"+_taskSHA+"' WHERE [TaskName] = '"+PROCSTORE.getState().names[_uid]+"'";
				reqMSSQL(update_task).then(function(){
					if(PROCSTORE.getState().tasks[_uid].type !== 'synchronization'){
						res('Служебная таблица [dbo].[ICS_Task] обновлена!');
						LOGGER.debug('MSSQL ICSTASK DONE');
					}else {
						let truncate = "TRUNCATE TABLE "+PROCSTORE.getState().tasks[_uid].table.name+"; DELETE FROM [dbo].[ICS_Hash] WHERE [TaskName] = '"+PROCSTORE.getState().names[_uid]+"'";
						reqMSSQL(truncate).then(function(){
							res('Таблица '+PROCSTORE.getState().tasks[_uid].table.name+' очищена! Хэши задачи '+PROCSTORE.getState().names[_uid]+' удалены!');
							LOGGER.debug('MSSQL ICSTASK DONE');
						}).catch(function(err3){
							rej('Таблица '+PROCSTORE.getState().tasks[_uid].table.name+' ошибка очистки: ' + err3);
							LOGGER.debug('MSSQL ICSTASK DONE');
						});
					}
				}).catch(function(err2){
					rej('Служебная таблица [dbo].[ICS_Task] ошибка обновления: ' + err2);
					LOGGER.debug('MSSQL ICSTASK DONE');
				});
			} else if(flag === 'done'){
				let update_task = "UPDATE [dbo].[ICS_Task] SET [Epoch] = '"+Date.now()+"' WHERE [TaskName] = '"+PROCSTORE.getState().names[_uid]+"'";
				reqMSSQL(update_task).then(function(){
					res('Служебная таблица [dbo].[ICS_Task] обновлена!');
					LOGGER.debug('MSSQL ICSTASK DONE');
				}).catch(function(err2){
					rej('Служебная таблица [dbo].[ICS_Task] ошибка обновления: '+err2);
					LOGGER.debug('MSSQL ICSTASK DONE');
				});
			} else {
				res('ok');
				LOGGER.debug('MSSQL ICSTASK DONE');
			}
		}).catch(function(err){
			if(flag === 'run'){
				let insert_task = "INSERT INTO [dbo].[ICS_Task] ([TaskName], [Hash]) VALUES ('"+PROCSTORE.getState().names[_uid]+"','"+_taskSHA+"')";
				reqMSSQL(insert_task).then(function(){
					if(PROCSTORE.getState().tasks[_uid].type !== 'synchronization'){
						res('Служебная таблица [dbo].[ICS_Task] обновлена!');
						LOGGER.debug('MSSQL ICSTASK DONE');
					}else {
						let truncate = "TRUNCATE TABLE "+PROCSTORE.getState().tasks[_uid].table.name+"; DELETE FROM [dbo].[ICS_Hash] WHERE [TaskName] = '"+PROCSTORE.getState().names[_uid]+"'";
						reqMSSQL(truncate).then(function(){
							res('Таблица '+PROCSTORE.getState().tasks[_uid].table.name+' очищена! Хэши задачи '+PROCSTORE.getState().names[_uid]+' удалены!');
							LOGGER.debug('MSSQL ICSTASK DONE');
						}).catch(function(err3){
							rej('Таблица '+PROCSTORE.getState().tasks[_uid].table.name+' ошибка очистки: ' + err3);
							LOGGER.debug('MSSQL ICSTASK DONE');
						});
					}
				}).catch(function(err2){
					rej('Служебная таблица [dbo].[ICS_Task] ошибка обновления: '+err2);
					LOGGER.debug('MSSQL ICSTASK DONE');
				});
			} else if (flag === 'done'){
				let insert_task = "INSERT INTO [dbo].[ICS_Task] ([TaskName], [Hash], [Epoch]) VALUES ('"+PROCSTORE.getState().names[_uid]+"','"+_taskSHA+"', '"+Date.now()+"')";
				reqMSSQL(insert_task).then(function(){
					res('Служебная таблица [dbo].[ICS_Task] обновлена!');
					LOGGER.debug('MSSQL ICSTASK DONE');
				}).catch(function(err2){
					rej('Служебная таблица [dbo].[ICS_Task] ошибка обновления: '+err2);
					LOGGER.debug('MSSQL ICSTASK DONE');
				});
			} else {
				rej("Неизвестная ошибка работы с [dbo].[ICS_Task]: "+err);
				LOGGER.debug('MSSQL ICSTASK DONE');
			}
		});
	});
}

//функция обновления версии
function updateVersion(_param, _version){
	LOGGER.debug('MSSQL UPDATEVERSION START');
	return new Promise(function(res, rej){
		reqMSSQL("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+_param+"'").then(function(val_select){
			let nex_req = "INSERT INTO [dbo].[T_SystemInfo] ([ParamName], [ParamValue]) VALUES ('"+_param+"', '"+_version+"')";
			if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
				nex_req = "UPDATE [dbo].[T_SystemInfo] SET [ParamValue] = '"+_version+"' WHERE [ParamName] = '"+_param+"'";
			}
			reqMSSQL(nex_req).then(function(val_next){
				res(val_next);
				LOGGER.debug('MSSQL UPDATEVERSION DONE');
			}).catch(function(err){
				rej(err);
				LOGGER.debug('MSSQL UPDATEVERSION DONE');
			});
		}).catch(function(err){
			rej(err);
			LOGGER.debug('MSSQL UPDATEVERSION DONE');
		});
	});
}

//получение конфигурации
function getConfig(){
	LOGGER.debug('MSSQL GETCONFIGS START');
	/*
		Логин как MD5(username),
		Пароль как SHA512('iRetail'+password)
	*/
	return new Promise(function(res, rej){
		reqMSSQL("SELECT TOP 2 [ParamName],[ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = 'ICS_Login' OR [ParamName] = 'ICS_Password'").then(function(val_select){
			if(Array.isArray(val_select) && (val_select.length === 2)){
				let pair = {
					login: '',
					password: ''
				};
				for(let i = 0; i < val_select.length; i++){
					switch(val_select[i].ParamName.value){
						case 'ICS_Login':
							pair.login = val_select[i].ParamValue.value;
							break;
						case 'ICS_Password':
							pair.password = val_select[i].ParamValue.value;
							break;
					}
				}
				res(pair);
				LOGGER.debug('MSSQL GETCONFIGS DONE');
			} else {
				LOGGER.debug(new Error('Отсутствуют данные для авторизации на сервере, добавьте ICS_Login и ICS_Password в таблицу T_SystemInfo!'));
				res();
				LOGGER.debug('MSSQL GETCONFIGS DONE');
			}
		}).catch(function(err){
			rej('MSSQL->' + err);
			LOGGER.debug('MSSQL GETCONFIGS DONE');
		});
	});
}

if(CONFIG.usedb !== false){
	mainmodule.connect = connMSSQLFirst;
	mainmodule.bulk = bulkMSSQL;
	mainmodule.req = reqMSSQL;
	mainmodule.multireq = multiReqMSSQL;
	mainmodule.testwt = testWorkTable;
	mainmodule.icstasks = icsTasks;
	mainmodule.version = updateVersion;
	mainmodule.getconfig = getConfig;
} else {
	mainmodule.connection = { reset: function(callback){return callback();} };
	mainmodule.connect = function(){ return new Promise(function(resolve){ resolve({}); })};
	mainmodule.bulk = function(){ return new Promise(function(resolve){ resolve({}); })};
	mainmodule.req = function(){ return new Promise(function(resolve){ resolve({}); })};
	mainmodule.multireq = function(){ return new Promise(function(resolve){ resolve({}); })};
	mainmodule.testwt = function(){ return new Promise(function(resolve){ resolve(); })};
	mainmodule.icstasks = function(){ return new Promise(function(resolve){ resolve(); })};
	mainmodule.version = function(){ return new Promise(function(resolve){ resolve(); })};
	mainmodule.getconfig = function(){ return new Promise(function(resolve){ resolve(); })};
}

module.exports = mainmodule;