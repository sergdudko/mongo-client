/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	PROCESS
 *  TASK-WORKER
 *	Процесс, исполняющий задачи
 *
 */
 
"use strict"

 var PATH = require('path');

if(parseInt(process.version.split(".")[0].substr(1,2), 10) < 10){
	Promise = require("bluebird");
}

//ловим эксепшны
var BUGTRACKER = require(PATH.join(__dirname, 'module.bugtracker.js'));	
BUGTRACKER(); 

//запускаю контроль RAM
var MEMCTRL = require(PATH.join(__dirname, 'module.memctrl.js'));	
MEMCTRL();

//подгружаемые библиотеки
var CLUSTER = require('cluster');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js')),
	TASKS = require(PATH.join(__dirname, 'module.tasks.js'));	
	
//разрыв IPC, уничтожаю процесс для определенности состояния
PROCSTORE.stderr = function(err){ 
	LOGGER.warn(err);
	if(err.indexOf('This socket has been ended by the other party') !== -1){	//ошибка отправки сообщения в сокет, безусловный выход
		setTimeout(function(){
			process.exit(1); 
		}, 5000);
	} else if(err.indexOf('connect ENOENT') !== -1){	//ошибка подключения к сокету или разрыв соединения, выход если не восстановится за 10 сек
		setTimeout(function(){
			if(PROCSTORE.connected !== true)
				process.exit(1); 
		}, 10000);
	}
}
	
startWorker(false);

function startWorker(){
	MSSQL.connect().then(function(){
		PROCSTORE.dispatch({type:"STATUS_WORKER", payload:{id:CLUSTER.worker.id, status:'idle'}});
		process.on("message", function(_data){	//получение новых сообщений в воркер
			if(_data.cmd === "TASK_TO_WORKER"){
				const _uid = _data.uid;
				LOGGER.log('TASK-> Задача '+ PROCSTORE.getState().names[_uid]+' запущена!');
				let timest = Date.now();
				MSSQL.connection.reset(function(){	//сброс соединения
					startTask(_uid).then(function(){
						LOGGER.log('TASK-> Задача '+ PROCSTORE.getState().names[_uid]+' завершена успешно за '+parseInt((Date.now()-timest)/1000)+'sec.!');
						MSSQL.icstasks(_uid, "done").then(function(val){
							if(typeof(val) !== 'undefined'){
								LOGGER.log('MSSQL-> '+val);
							}
						}).catch(function(err){
							LOGGER.warn('MSSQL-> '+err);
						}).finally(function(){
							LOGGER.debug('Задача '+ PROCSTORE.getState().names[_uid]+' завершена успешно за '+parseInt((Date.now()-timest)/1000)+'sec.!');
							PROCSTORE.dispatch({type:'STOP_TASK', payload: {uid:_uid, worker:CLUSTER.worker.id, complete: true}});
						});
					}).catch(function(error){
						LOGGER.error('TASK-> Задача '+ PROCSTORE.getState().names[_uid]+' завершена за '+parseInt((Date.now()-timest)/1000)+'sec. с ошибкой:'+error);
						PROCSTORE.dispatch({type:'STOP_TASK', payload: {uid:_uid, worker:CLUSTER.worker.id}});
					});
				});
			}
		});
	}).catch(function(error){
		LOGGER.error('MSSQL-> Ошибка подключения: '+error);
		setTimeout(startWorker, 15000);
	}).finally(function(){
		LOGGER.debug('PROCESS TASK STARTWORKER START');
	});
}

function startTask(uid){
	LOGGER.debug('PROCESS TASK STARTTASK START');
	const _uid = uid;
	return new Promise(function(resolve, reject){
		if((typeof(PROCSTORE.getState().tasks[_uid].type) === 'string') && (typeof(TASKS[PROCSTORE.getState().tasks[_uid].type]) !== 'undefined')){
			MSSQL.testwt().then(function(){
				return MSSQL.icstasks(_uid, "run");
			}).then(function(val){
				if((typeof(val) !== 'undefined') && (val !== 'ok')){
					LOGGER.log('MSSQL-> '+val);
				}
				TASKS[PROCSTORE.getState().tasks[_uid].type](_uid).then(function(val3){
					resolve(val3);
					LOGGER.debug('PROCESS TASK STARTTASK DONE');
				}).catch(function(err3){
					reject(err3);
					LOGGER.debug('PROCESS TASK STARTTASK DONE');
				});
			}).catch(function(err){
				if(err !== 'error'){
					LOGGER.error('MSSQL-> Проблема с служебными таблицами: '+err);
				}
				reject(err);
				LOGGER.debug('PROCESS TASK STARTTASK DONE');
			});
		} else {
			reject(new Error('Не известный тип задачи!'));
			LOGGER.debug('PROCESS TASK STARTTASK DONE');
		}
	});
}