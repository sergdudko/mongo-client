/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	PROCESS
 *  SUB-WORKER
 *	Вспомогательный процесс клиента, отправляет ошибки на почту и читает таски из папки
 *
 */
 
"use strict"

var PATH = require('path');

if(parseInt(process.version.split(".")[0].substr(1,2), 10) < 10){
	Promise = require("bluebird");
}

//ловим эксепшны
var BUGTRACKER = require(PATH.join(__dirname, 'module.bugtracker.js'));	
BUGTRACKER(); 

//запускаю контроль RAM
var MEMCTRL = require(PATH.join(__dirname, 'module.memctrl.js'));	
MEMCTRL();

//подгружаемые библиотеки
var FS = require('fs'),
	LODASH = require('lodash');
	
//подгружаемые модули
var SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));	

//разрыв IPC, уничтожаю процесс для определенности состояния
PROCSTORE.stderr = function(err){ 
	LOGGER.warn(err);
	if(err.indexOf('This socket has been ended by the other party') !== -1){	//ошибка отправки сообщения в сокет, безусловный выход
		setTimeout(function(){
			process.exit(1); 
		}, 5000);
	} else if(err.indexOf('connect ENOENT') !== -1){	//ошибка подключения к сокету или разрыв соединения, выход если не восстановится за 10 сек
		setTimeout(function(){
			if(PROCSTORE.connected !== true)
				process.exit(1); 
		}, 10000);
	}
}

//чтение T_SystemInfo в стор
function updateVariable(){
	LOGGER.debug('PROCESS SUB UPDATEVARIABLE START');
	return new Promise(function(res, rej){
		MSSQL.req("SELECT * FROM [dbo].[T_SystemInfo] WITH (NOLOCK)").then(function(val){ 
			let _val = {};
			for(const key in val){
				_val[val[key].ParamName.value] = val[key].ParamValue.value;
			}
			PROCSTORE.dispatch({type:'UPDATE_VARIABLE', payload: _val});
			setTimeout(function(){ 
				res(true);
				LOGGER.debug('PROCESS SUB UPDATEVARIABLE DONE');
			}, 5000);
		}).catch(function(err){
			LOGGER.warn('MSSQL-> Ошибка получения переменных: '+err);
			rej(err);
			LOGGER.debug('PROCESS SUB UPDATEVARIABLE DONE');
		});
	});
}

//тихий режим
function dateFromSilent(_arg){
	let _d = new Date();
	let _start = _arg.split(":");
	let _datestart;
	let _hours = 0, _minutes = 0, _seconds = 0;
	switch(_start.length){
		case 1:		//заданы только часы
			_hours = parseInt(_start[0], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		case 2:		//заданы часы:минуты
			_hours = parseInt(_start[0], 10);
			_minutes = parseInt(_start[1], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		case 3:		//заданы часы:минуты:секунды
			_hours = parseInt(_start[0], 10);
			_minutes = parseInt(_start[1], 10);
			_seconds = parseInt(_start[2], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		default:
			throw new Error('Не корректный формат silent!');
	}
	if(typeof(_datestart) !== 'object')
		throw new Error('Не корректный формат silent!');
	else
		return _datestart;
}

//функция обновления всех заданий (парсит задания из файлов в папке /tasks/ и передает в хранилище, чистая)
function checkTaskDir(){
	LOGGER.debug('PROCESS SUB CHECKTASKDIR START');
	FS.readdir(PATH.join(__dirname, '/tasks/'), function(err,filesall){
		try{
			if(err){
				throw err;
			} else {
				let files = [];
				for(let key = 0; key < filesall.length; key++){ 
					if(filesall[key].substr(-5).toString() === '.json'){	//выбираем только файлы .json
						files.push(filesall[key]);
					}
				}
				let tasks = {}, names = {};
				let flag = 0;
				for(let key = 0; key < files.length; key++){
					let filename = files[key];
					FS.readFile(PATH.join(__dirname, '/tasks/', filename), "utf8", (err, data) => {
						try{
							let _data = "";
							if (err) {
								throw err;
							} else {
								const _tmp = data.split("***");
								_data = _tmp[0];	//первый элемент выборки
								for(let i = 1; (i+1) < _tmp.length; i=i+2){
									const _var = PROCSTORE.getState().variable[_tmp[i]]; //всегда первый элемент выборки
									if(typeof(_var) !== 'undefined'){
										if(_data[_data.length-1] !== '"')
											_data = _data + '"';
										_data = _data + _var;
										if(_tmp[i+1][0] !== '"')	//всегда второй элемент выборки
											_data = _data + '"';
										_data = _data + _tmp[i+1];
									}else 
										throw new Error("Неизвестная переменная "+_tmp[i]+"!");
								}
								let _object = JSON.parse(_data);
								if(_object.silent){
									let _silent = _object.silent.replace(/ /gi, "");
									let _silentarr = _silent.split(';');
									for(let _i = 0; _i < _silentarr.length; _i++){
										if(_silentarr[_i] !== ""){
											let _silentinterval = _silentarr[_i].split('-');
											if(_silentinterval.length !== 2){
												throw new Error('Не корректный диапазон silent, формат должен быть 01:05-10:00;11:00-12:00; !');
											} else{
												let _tmpinterval = {
													start: dateFromSilent(_silentinterval[0]).getTime(), 
													end: dateFromSilent(_silentinterval[1]).getTime()
												};
												if(_tmpinterval.start > _tmpinterval.end){
													throw new Error('Не корректный формат silent, время начала не может быть больше времени окончания!');
												}
												if(!Array.isArray(_object.silent)){
													_object.silent = [_tmpinterval];
												} else{
													_object.silent.push(_tmpinterval);
												}
											}
										}
									}
									if(Array.isArray(_object.silent)){
										_object.silent = LODASH.sortBy(_object.silent, 'start');
									}
								}
								let _valid = FUNCTIONS.taskvalidator(_object);
								if(_valid === true){
									tasks[FUNCTIONS.hasher(filename)] = _object; 
									names[FUNCTIONS.hasher(filename)] = filename;
								} else {
									throw new Error(_valid);
								}
							}
						} catch(err){
							LOGGER.error("SUB-> Ошибка при чтении таска "+filename+":" + err);
						}
						flag++;
						if(flag === (files.length)){
							PROCSTORE.dispatch({type:'UPDATE_TASKS', payload: {"tasks":tasks, "names":names}});
							LOGGER.debug('PROCESS SUB CHECKTASKDIR DONE');
						}
					});
				}
			}
		} catch(err){
			LOGGER.error("SUB-> Ошибка при чтении каталога с тасками:" + err);
		}
	});
}

	
SENDMAIL.worker();
startWorker();
//автозавершение процесса для обновления (по версии в package.json)
FS.watch(PATH.join(__dirname, 'package.json'), { encoding: 'buffer' }, (eventType) => {
	try{
		if(eventType === 'change'){
			let newVERSION;
			setTimeout(function(){
				FS.readFile(PATH.join(__dirname, 'package.json'), "utf8", (err, data) => {
					try{
						if(err) throw err;
						newVERSION = JSON.parse(data).version;
					} catch(err){
						LOGGER.warn(err);
					}
					if(newVERSION !== PROCSTORE.getState().version){
						LOGGER.warn('UPDATER-> Блокирую запуск задач, т.к. ожидаю обновление до версии '+newVERSION);
						PROCSTORE.dispatch({type:'UPDATE_IRCC', payload: newVERSION});
					}
				});
			}, 60000);
		}
	} catch(err){
		LOGGER.warn(err);
	}
});

function startWorker(){
	MSSQL.connect().then(function(){
		let _timer = function(){
			let _timeout = 300000;
			LOGGER.debug('PROCESS SUB UPDATEVARIABLE START');
			updateVariable().then(function(){
				checkTaskDir();
			}).catch(function(err){
				_timeout = 15000;
				LOGGER.warn('MSSQL-> Ошибка обновления переменных: '+err);
			}).finally(function(){
				setTimeout(_timer, _timeout);
				LOGGER.debug('PROCESS SUB UPDATEVARIABLE DONE');
			});
		}
		setTimeout(function(){
			MSSQL.version("iRetailCloudClient", PROCSTORE.getState().version).catch(function(err){
				LOGGER.warn("SUB-> Ошибка обновления версии приложения: "+err);
			}).finally(function(){
				setTimeout(_timer, 1000);
			});
		}, 1000);
	}).catch(function(error){
		LOGGER.error('MSSQL-> Ошибка подключения: '+error);
		setTimeout(startWorker, 15000);
	}).finally(function(){
		LOGGER.debug('PROCESS SUB STARTWORKER START');
	});
}