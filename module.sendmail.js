/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  SENDMAIL
 *	Отправка сообщений на email. Функция SendMailOne - складирует сообщения в папку /mail, воркер SendMailWorker забирает и отправляет.
 *
 */

"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	NODEMAILER = require('nodemailer'),
	CRYPTO=require("crypto"),
	FS=require("fs"),
	PATH = require('path'),
	OS = require('os');

//глобальные переменные
var stdout = console;
var run = 0;
var identifier = '';
let SendMail;


//валидация конфига email
function ConfValidator(){
	try{
		if( (typeof(CONFIG.email) === 'object') &&
			(typeof(CONFIG.email.host) === 'string') &&
			(typeof(CONFIG.email.port) === 'number') &&
			(typeof(CONFIG.email.login) === 'string') &&
			(typeof(CONFIG.email.password) === 'string') &&
			(typeof(CONFIG.email.mailto) === 'string') ){
				return true;
			} else {
				return false;
			}
	} catch(err){
		if(CONFIG.debug){
			stdout.error('SENDMAIL-> ошибка валидации конфига: '+err);
		}
		return false;
	}
}

if(ConfValidator()){
	var _secure = false;
	if (CONFIG.email.port === 465) { _secure = true; }
	if(process.env.computername)
		identifier = identifier+'COMPUTERNAME:'+process.env.computername;
	if((identifier === '') || (process.env.computername !== OS.hostname())){
		if(OS.hostname())
			identifier = identifier+'; HOSTNAME:'+OS.hostname();
	}
	try{
		let _interfaces = OS.networkInterfaces();
		identifier = identifier+'; ADDRESSES: ';
		for(const _interface in _interfaces){
			let _intrfc = _interfaces[_interface];
			if(Array.isArray(_intrfc)){
				for(let _i=0; _i < _intrfc.length; _i++){
					let _t = _intrfc[_i];
					if((typeof(_t) === 'object') && (_t.internal === false) && (_t.family === 'IPv4') && (typeof(_t.address) === 'string')){
						identifier = identifier + _t.address+'; ';
					}
				}
			}
		}
	} catch(e){
		console.log(e);
	}
	let transporter = NODEMAILER.createTransport({
		host: CONFIG.email.host,
		port: CONFIG.email.port,
		secure: _secure, // true for 465, false for other ports
		auth: {
			user: CONFIG.email.login, 
			pass: CONFIG.email.password 
		}
	});

	SendMail = function (_obj){
		var obj = LODASH.clone(_obj);
		if(typeof(obj.mailto) === 'undefined'){
			obj.mailto = CONFIG.email.mailto;
		}
		if(typeof(obj.theme) === 'undefined'){
			obj.theme = obj.message.split(" ").slice(0, 10).join(" ");
		}
		let mailOptions = {
			from: '"iRetail Cloud Client" <'+CONFIG.email.login+'>', // адрес отправителя
			to: obj.mailto, // адреса получателей, через запятую
			subject: obj.theme, // тема
			text: identifier+'\n\n'+obj.message, // текст
			//html: obj.message // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, (error, info) => {
			run = 0;
			if (error) {
				return stdout.error('SENDMAIL-> ошибка отправки почты: '+error);
			} else {
				stdout.log('SENDMAIL->Сообщение отправлено с id: %s', info.messageId);
			}
		});
	}
} else {
	SendMail = function (){
		return stdout.warn('SENDMAIL-> настройки почты некорректны, уведомления отключены');
	}
}

//добавить сообщение в очередь
function SendMailOne(_obj, _stdout){
	if(ConfValidator()){
		if(typeof(_stdout) === 'object'){
			for(const key in _stdout){
				if(typeof(_stdout[key]) === 'function'){
					stdout[key] = _stdout[key];
				}
			}
		}
		if(typeof(_obj) === 'object'){
			if(typeof(_obj.message) === 'string'){
				try{
					const hash = CRYPTO.createHash('sha1');
					var data = JSON.stringify(_obj);
					hash.update(data);
					FS.writeFile(PATH.join(__dirname, '/mail/', Date.now()+'-'+hash.digest('hex')+'.mail'), data, (err) => {
						try{
							if (err) {
								throw err;
							}
						} catch(e){
							stdout.error('SENDMAIL-> ошибка записи сообщения в очередь шаг2: '+e);
						}
					});
				} catch(err){
					stdout.error('SENDMAIL-> ошибка записи сообщения в очередь шаг1: '+err);
				}
			}
		}
	} else {
		stdout.warn('SENDMAIL-> настройки почты некорректны, уведомления отключены');
	}
}

//парсит очередь сообщений
function goQUERY(){
	if((run+300000) < Date.now()){
		run = Date.now();
		FS.readdir(PATH.join(__dirname, '/mail/'), function(err,mailall){
			try{
				if(err){
					throw err;
				} else {
					if(typeof(mailall[0]) !== 'undefined'){
						var message = mailall[0];
						FS.readFile(PATH.join(__dirname, '/mail/', message), "utf8", (err1, data) => {
							try{
								if(err1){
									throw err1;
								} else {
									SendMail(JSON.parse(data));
								}
							} catch(e){
								stdout.error('SENDMAIL-> ошибка загрузки сообщения из очереди: '+e);
								run = 0;
							}
							FS.unlink(PATH.join(__dirname, '/mail/', message), (err2) => {
								if(err2){
									stdout.error('SENDMAIL-> ошибка удаления сообщения из очереди: '+err2);
								}
							});
						});
					} else {
						run = 0;
					}
				}
			} catch(e){
				stdout.error('SENDMAIL-> ошибка чтения директории сообщений: '+e);
				run = 0;
			}
		});
	}
}

var SendMailWorker = function(){setInterval(goQUERY, 1000);}

module.exports.send = SendMailOne;
module.exports.worker = SendMailWorker;
module.exports.identifier = identifier;