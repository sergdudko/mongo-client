/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE UNLOADPROCEDURE
 *	Генерация JSON (функции и хранилки) для отправки в облако
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));
	
function unloadprocedure(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK UNLOADPROCEDURE START');
	return new Promise(function(res, rej){
		const _uid = uid;
		const taskname = ''+PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		/* -> конфигурация */
		let step = 0;
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK UNLOADPROCEDURE DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		try{
			MSSQL.req("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+task.version+"'").then(function(val_select){
				let req = "Select TOP 10 Cast(Temp as nvarchar(max)) as TextData,ResultTable.ROUTINE_NAME,ResultTable.ROUTINE_TYPE,ResultTable.LAST_ALTERED from (\
								Select (select cast(TextData.[text] as nvarchar(4000)) from sys.syscomments  as TextData\
										Inner Join sys.sysobjects SO ON TextData.id=SO.id\
										where SO.name=TableTemp.ROUTINE_NAME \
										order by colid\
										FOR XML PATH(''), TYPE\
										).value('.','VARCHAR(MAX)') as Temp,* from (\
								SELECT distinct information_schema.routines.[ROUTINE_NAME], information_schema.routines.[ROUTINE_TYPE],  information_schema.routines.[LAST_ALTERED] \
									FROM ((information_schema.routines \
									INNER JOIN sys.sysobjects ON sys.sysobjects.[name] = information_schema.routines.[ROUTINE_NAME]) \
									INNER JOIN sys.syscomments ON sys.syscomments.[id] = sys.sysobjects.[id])\
									WHERE information_schema.routines.[ROUTINE_CATALOG] LIKE '"+CONFIG.db.database+"' \
								) as TableTemp\
							) as ResultTable ORDER BY [LAST_ALTERED]";
				let lastproc, lasttime;
				if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
					let version = val_select[0]['ParamValue'].value.split("|");
					lastproc = version[0];
					lasttime = version[1];
					req = "Select TOP 10 Cast(Temp as nvarchar(max)) as TextData,ResultTable.ROUTINE_NAME,ResultTable.ROUTINE_TYPE,ResultTable.LAST_ALTERED from (\
									Select (select cast(TextData.[text] as nvarchar(4000)) from sys.syscomments  as TextData\
											Inner Join sys.sysobjects SO ON TextData.id=SO.id\
											where SO.name=TableTemp.ROUTINE_NAME \
											order by colid\
											FOR XML PATH(''), TYPE\
											).value('.','VARCHAR(MAX)') as Temp,* from (\
									SELECT distinct information_schema.routines.[ROUTINE_NAME], information_schema.routines.[ROUTINE_TYPE],  information_schema.routines.[LAST_ALTERED] \
										FROM ((information_schema.routines \
										INNER JOIN sys.sysobjects ON sys.sysobjects.[name] = information_schema.routines.[ROUTINE_NAME]) \
										INNER JOIN sys.syscomments ON sys.syscomments.[id] = sys.sysobjects.[id])\
										WHERE information_schema.routines.[ROUTINE_CATALOG] LIKE '"+CONFIG.db.database+"' AND information_schema.routines.[LAST_ALTERED]>=cast('"+lasttime+"' as datetime) \
									) as TableTemp\
								) as ResultTable ORDER BY [LAST_ALTERED]";
				}
				MSSQL.req(req).then(function(val_select2){ 	//в state счетчик ошибок
					if((typeof(val_select2) === 'object') && (!LODASH.isEqual(val_select2, []))){
						let oneStepTASK = function(iteration, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
							if(typeof(val_select2[iteration]) === 'undefined'){
								res(_uid);
								logger.debug('TASK UNLOADPROCEDURE DONE');
							} else {
								if(iteration_bad === 0)
									step++;
								if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение.
									if(lastproc !== val_select2[iteration]['ROUTINE_NAME'].value){
										let _data = JSON.stringify([{
											"guid" : FUNCTIONS.hasher(val_select2[iteration]['ROUTINE_NAME'].value), 
											"type" : "Документ.ХранимаяПроцедура", 
											"description" : val_select2[iteration]['ROUTINE_NAME'].value, 
											"date" : FUNCTIONS.toJSONn(val_select2[iteration]['LAST_ALTERED'].value),
											"files" : [
												{	
													"name":	val_select2[iteration]['ROUTINE_NAME'].value,
													"requisites":[
														{
															"type" : "Строка", 
															"id" : "body", 
															"name" : "body", 
															"value" : Buffer.from(val_select2[iteration]['TextData'].value, 'utf8').toString('base64')
														},
														{
															"type" : "Строка", 
															"id" : "encoding", 
															"name" : "encoding", 
															"value" : "base64"
														},
														{
															"type" : "Строка", 
															"id" : "type", 
															"name" : "type", 
															"value" : val_select2[iteration]['ROUTINE_TYPE'].value
														},
														{
															"type" : "Строка", 
															"id" : "sha1", 
															"name" : "sha1", 
															"value" : FUNCTIONS.hasher(val_select2[iteration]['TextData'].value)
														}
													]
												}
											]
										}]);
										let _date = FUNCTIONS.toJSONn().replace(/T/gi, " ").replace(/Z/gi, "");
										let _version = val_select2[iteration]['ROUTINE_NAME'].value + "|" + val_select2[iteration]['LAST_ALTERED'].value.toJSON().replace(/T/gi, " ").replace(/Z/gi, "");
										let next_req = "INSERT INTO [dbo].[T_PostMessages] ( [Body], [Time], [State] ) VALUES ('"+_data+"', '"+_date+"', '0' )";
										MSSQL.req(next_req).then(function(){
											return MSSQL.version(task.version, _version).catch(function(err){ logger.error('Ошибка обновления версии: '+ err); });	//обновляю версию
										}).then(function(){
											setTimeout(oneStepTASK, 1, ++iteration, 0);
										}).catch(function(err){
											setTimeout(oneStepTASK, 1, iteration, ++iteration_bad);
											logger.warn("MSSQL-> Ошибка добавления сообщения в T_PostMessages: "+err);
										});
									} else {
										setTimeout(oneStepTASK, 1, ++iteration, 0);
									}
								} else {
									exit();
								}
							}
						}
						setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), 0, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
					} else {
						res(_uid);
						logger.debug('TASK UNLOADPROCEDURE DONE');
					}
				}).catch(function(err){
					rej("Не могу прочитать таблицу T_PostMessages: "+err);
					logger.debug('TASK UNLOADPROCEDURE DONE');
				});
			}).catch(function(err){
				rej("Не могу прочитать версию для "+task.version+": "+err);
				logger.debug('TASK UNLOADPROCEDURE DONE');
			});
		} catch(err){	//ошибка глобальной обертки функции
			rej(err);
			logger.debug('TASK UNLOADPROCEDURE DONE');
		}
	});
}

module.exports = unloadprocedure;