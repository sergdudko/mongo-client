/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  PROCSTORE
 *	Хранилище состояний процессов (использует IPC), передача методом worker->master->workers[]	
 *  движок вынес в либу redux-cluster
 */

"use strict"

//подгружаемые библиотеки
var REDUXCLUSTER = require('redux-cluster'),
	CLUSTER = require('cluster'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js'));

//инициализируем хранилище
var ProcessStorage = REDUXCLUSTER.createStore(editProcessStorage);
ProcessStorage.mode = "snapshot";

//редьюсер
function editProcessStorage(state = {
	run:{}, 	//объект uid{worker:(number), timestamp:(number)}
	tasks:{}, 	//пара uid - текст тасков
	logs:{}, 	//пара uid - таймштамп успешного выполнения
	names:{},	//пара uid - имя файла, причем uid это hash имени файла
	worker:{},	//пара воркер - состояние (только для task-воркеров)
	status:{	//состояние воркеров
		mssql:{}
	},	
	version:'',	//версия приложения
	variable:{},	//переменные из T_SystemInfo
	update: '',	//флаг обновления
	updatetime: 0,	//время установки флага обновления
	query:[]		//приоритеты очереди
}, action){
	let state_new = LODASH.clone(state);
	try { 
		let _t, newquery;
		switch (action.type){
			case 'STATUS_MSSQL':
				state_new.status.mssql[action.payload.worker] = action.payload.status;
				return state_new;
			case 'STOP_TASK':
				delete state_new.run[action.payload.uid];
				state_new.worker[action.payload.worker] = 'idle';
				if(action.payload.complete){
					state_new.logs[action.payload.uid] = Date.now();
				}
				return state_new;
			case 'RUN_TASK':		//запуск задачи
				state_new.run[action.payload.uid] = {
					worker:action.payload.worker,
					timestamp: Date.now()
				};
				state_new.worker[action.payload.worker] = 'busy';
				_t = state_new.query.indexOf(action.payload.uid);
				if(_t !== -1)		//если присутствует, удаляю из массива
					state_new.query.splice(_t, 1);
				state_new.query.push(action.payload.uid);	//добавляю в массив (снижаю приоритет)
				return state_new;
			case 'CLEAR_TASK':		//очистка задач умерших воркеров
				repairStore(state_new, true);
				return state_new;
			case 'UPDATE_TASKS':	//обновление данных по таскам
				state_new.tasks = action.payload.tasks;
				state_new.names = action.payload.names;
				newquery = [];
				for(let i = 0; i < state_new.query.length; i++){	//удаляю из очереди несуществующие таски
					if(typeof(state_new.names[state_new.query[i]]) !== 'undefined')
						newquery.push(state_new.query[i]);
				}
				state_new.query = newquery;
				for(const key in state_new.names){	//добавляю в очередь новые таски
					let _i = state_new.query.indexOf(key);
					if(_i === -1)
						state_new.query.push(key);
				}
				for(const key in state_new.logs){	//удаляю логи несуществующих задач
					if(typeof(state_new.names[key]) === 'undefined'){
						delete state_new.logs[key];
					}
				}
				repairStore(state_new, true);
				return state_new;
			case 'STATUS_WORKER':	//обновление статуса воркера
				state_new.worker[action.payload.id] = action.payload.status;
				repairStore(state_new);
				return state_new;
			case 'SET_VERSION':		//установка версии приложения (старт приложения)
				state_new.version = action.payload;
				state_new.run = {};
				state_new.update = state_new.version;
				state_new.updatetime = 0;
				repairStore(state_new);
				return state_new;
			case 'UPDATE_VARIABLE':
				for(const key in action.payload){
					state_new.variable[key] = action.payload[key];
				}
				return state_new;
			case 'CLEAR_LOG':
				state_new.logs = {};
				return state_new;
			case 'CLEAR_LOG_TASK':
				if(typeof(state_new.logs[action.payload]) !== 'undefined'){
					delete state_new.logs[action.payload];
				}
				return state_new;
			case 'UPDATE_IRCC':
				state_new.update = action.payload;
				if(action.payload){
					state_new.updatetime = Date.now();
				}
				return state_new;
			default:
				break;
		}
	} catch(e){
		LOGGER.error("PROCSTORE-> "+e);
	}
	return state_new;
}

function repairStore(store, flag){
	if((process.mainModule.filename.indexOf('process.master.js') !== -1) && (CLUSTER.isMaster)){
		if(ProcessStorage.mode !== 'snapshot'){
			throw new Error('Хранилище должно быть в режиме snapshot!');
		}
		for(const id in store.worker){
			if(typeof(CLUSTER.workers[id]) === 'undefined'){
				delete store.worker[id];
			}
		}
		for(const sttype in store.status){
			for(const id in store.status[sttype]){
				if(typeof(CLUSTER.workers[id]) === 'undefined'){
					delete store.status[sttype][id];
				}
			}
		}
		for(const task in store.run){
			let id = store.run[task].worker;
			if((typeof(CLUSTER.workers[id]) === 'undefined') || (store.worker[id] === 'idle')){
				if(flag){
					LOGGER.error('Задача '+store.names[task]+' зависла и была отменена!');
				}
				delete store.run[task];
			}
		}
	}
	return;
}

module.exports = ProcessStorage;