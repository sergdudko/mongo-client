/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE DOWNLOADPROCEDURE
 *	Загрузка процедур из облака
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js')),
	SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js'));
	
function downloadprocedure(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK DOWNLOADPROCEDURE START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = ''+PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		const link = CONFIG.server.url+'/v3/query'; //ссылка получения данных
		/* -> конфигурация */
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK DOWNLOADPROCEDURE DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let auth;
		MSSQL.getconfig().then(function(val){
			try{
				if(val && val.login && val.password){
					auth = val.login + ':' + val.password;
				}
				let oneStepTASK = function(request, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
					if(iteration_bad === 0)
						step++;
					if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение (на 5 проходах были проблемы из-за статуса 206 - выполнялся долше 5 минут, увеличение же таймаута запроса затягивает выполнение)
						let _request = LODASH.clone(request);
						MSSQL.req("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+task.version+"'").then(function(val_select){ 	//получаю версию
							if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
								if(typeof(_request.find["version"]) !== 'object'){
									_request.find["version"] = {};
								}
								if(typeof(val_select[0]['ParamValue'].value) === 'string')
									_request.find["version"]["$gt"] = val_select[0]['ParamValue'].value;
							}
							let headers = {"ics-cache-timeout":task.cache};	//задаю хидеры
							let _restsettings = {
								url:link,
								method:"POST",
								data:_request,
								datatype:"object",
								headers:headers
							};
							if(typeof(auth) === 'string')
								_restsettings.auth = auth;
							FUNCTIONS.rest(_restsettings).then(function(req_data){	//выполняю POST запрос на сервер (упрощенный вариант, без 208 статуса и хидеров запроса)
								if(parseInt(parseInt(req_data[0])/100) === 2){
									if((req_data[0] === 200) || (req_data[0] === 201)){
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]);
									} else {
										let _str;
										try {
											if(typeof(req_data[1]) === 'object'){
												_str = JSON.stringify(req_data[1]);
											} else {
												_str = req_data[1];
											}
										} catch(_e){
											logger.warn(_e);
										}
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
									}
								}else{
									let _str;
									try {
										if(typeof(req_data[1]) === 'object'){
											_str = JSON.stringify(req_data[1]);
										} else {
											_str = req_data[1];
										}
									} catch(_e){
										logger.warn(_e);
									}
									logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
								}
								switch(req_data[0]){
									case 200:
									case 201:
										(function(){
											let oneUpdate = function(_i){
												if(_i < req_data[1].length){
													let _doc = req_data[1][_i];
													let _version = _doc.version;
													if(typeof(_version) === 'undefined'){
														logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Документ '+_doc["_id"]+' не содержит версии!');
														setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
													}else{
														let _file = {
															type: _doc.type,
															description: _doc.description,
															version: _doc.version,
															data: _doc.data,
															requisite: {}
														}
														if(Array.isArray(_doc.files)){
															for(const id in _doc.files){
																if(_doc.files[id].name === _file.description){
																	if(Array.isArray(_doc.files[id].requisites)){
																		for(const rid in _doc.files[id].requisites){
																			_file.requisite[_doc.files[id].requisites[rid].id] = _doc.files[id].requisites[rid].value;
																		}
																	}
																}
															}
														}
														if(_file.requisite["encoding"] === "base64"){ 
															_file.requisite["body"] = Buffer.from(_file.requisite["body"], "base64").toString("utf8"); 
														}
														let next_req = "Select TOP 10 Cast(Temp as nvarchar(max)) as TextData,ResultTable.ROUTINE_NAME,ResultTable.ROUTINE_TYPE,ResultTable.LAST_ALTERED from (\
																			Select (select cast(TextData.[text] as nvarchar(4000)) from sys.syscomments  as TextData\
																					Inner Join sys.sysobjects SO ON TextData.id=SO.id\
																						where SO.name=TableTemp.ROUTINE_NAME \
																						order by colid\
																						FOR XML PATH(''), TYPE\
																					).value('.','VARCHAR(MAX)') as Temp,* from (\
																			SELECT distinct information_schema.routines.[ROUTINE_NAME], information_schema.routines.[ROUTINE_TYPE],  information_schema.routines.[LAST_ALTERED] \
																				FROM ((information_schema.routines \
																				INNER JOIN sys.sysobjects ON sys.sysobjects.[name] = information_schema.routines.[ROUTINE_NAME]) \
																				INNER JOIN sys.syscomments ON sys.syscomments.[id] = sys.sysobjects.[id])\
																				WHERE information_schema.routines.[ROUTINE_CATALOG] LIKE '"+CONFIG.db.database+"' AND information_schema.routines.[ROUTINE_NAME] LIKE '"+_file.description+"' AND information_schema.routines.[ROUTINE_TYPE] LIKE '"+_file.requisite["type"]+"' \
																			) as TableTemp\
																		) as ResultTable ORDER BY [LAST_ALTERED]";
														MSSQL.req(next_req).then(function(val_select2){
															return new Promise(function(_res, _rej){
																let _update = true;
																if((typeof(val_select2) === 'object') && (!LODASH.isEqual(val_select2, []))){
																	logger.debug(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| SHA1 LOCAL: '+FUNCTIONS.hasher(val_select2[0]['TextData'].value) + " | SHA1 REMOTE: "+_file.requisite['sha1']);
																	if(FUNCTIONS.hasher(val_select2[0]['TextData'].value) === _file.requisite['sha1']){
																		_update = false;
																	}
																}
																if(!_update){
																	_res(true);
																} else {
																	let next_req2 = _file.requisite['body'];
																	let _flag = false;
																	MSSQL.connection.beginTransaction(function(err){
																		if(err){
																			logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не удалось начать транзакцию!');
																			_rej(err);
																		} else {
																			logger.debug(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| START TRANSACTION!');
																			const errorHandler = function(err){
																				MSSQL.connection.rollbackTransaction(function(_err){
																					logger.debug(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| ROLLBACK TRANSACTION!');
																					if(_err){
																						logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу закрыть транзакцию: '+ _err);
																					}
																					_rej(err);
																				});
																			}
																			const commitHandler = function(){
																				MSSQL.connection.commitTransaction(function(_err){
																					logger.debug(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| COMMIT TRANSACTION!');
																					if(_err){
																						logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Не могу зафиксировать транзакцию: '+ _err);
																						_rej(_err);
																					} else {
																						_res(true);
																					}
																				});
																			}
																			MSSQL.req(" IF EXISTS (SELECT * FROM information_schema.routines WHERE [ROUTINE_CATALOG] LIKE '"+CONFIG.db.database+"' AND [ROUTINE_NAME] LIKE '"+_file.description+"' AND [ROUTINE_TYPE] LIKE '"+_file.requisite["type"]+"') DROP "+_file.requisite["type"]+" ["+_file.description+"]").catch(function(err){
																				logger.warn(err);
																			}).finally(function(){
																				return MSSQL.req(next_req2);
																			}).then(function(){
																				_flag = true;
																				commitHandler();
																				SENDMAIL.send({
																					message:LOGGER.datetime()+'На сервере расходится SHA1-сумма хранимой '+_file.requisite["type"]+' '+_file.description+' с эталонной, попытка накатить процедуру была:'+(function(){if(_flag){ return 'УСПЕХ'; }else{ return 'ОШИБКА'; }})(), 
																					theme:'Расхождение хранимых процедур. Исправлено: '+(function(){if(_flag){ return 'да.'; }else{ return 'нет.'; }})()
																				});
																			}).catch(function(err){
																				errorHandler(err);
																			});
																		}
																	});
																}
															});
														}).then(function(){
															return MSSQL.version(task.version, _version).catch(function(err){ logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка обновления версии: '+ err); });	//обновляю версию
														}).then(function(){
															_i++;
															oneUpdate(_i);
														}).catch(function(err){
															logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| MSSQL-> Ошибка запроса: '+err);
															setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
														});
													}
												} else {
													setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), _request, 0); 
												}
											}
											oneUpdate(0);
										})();
										break;
									case 204:	//данных нет, задача завершена
										(function(){
											exit(true);
										})();
										break;
									case 206:	//данных нет
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); //запустим через 1 минуту, но добавим флаг не успешного выполнения чтобы исключить вероятность зависания
										})();
										break;
									default:
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
										})();
										break;
								}
							}).catch(function(error){	//ошибка в функции POST запроса
								logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+error);
								setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
							});
						}).catch(function(err){
							rej("Не могу прочитать версию для "+task.version+": "+err);
							logger.debug('TASK DOWNLOADPROCEDURE DONE');
						});
					} else {
						exit();
					}
				}
				const req = ({"find": task.request, "limit": task.limit, "skip":0, "sort":{"version": 1}, "requires": []});	//первый запуск сортировка по версии (это важно)
				setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), req, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
			} catch(err){	//ошибка глобальной обертки функции
				rej(err);
				logger.debug('TASK DOWNLOADPROCEDURE DONE');
			}
		}).catch(function(err){
			logger.warn(err);
			exit();
		});
	});
}

module.exports = downloadprocedure;