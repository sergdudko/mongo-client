/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  LOGGER
 *	Логгер приложения, отправляет ошибки на email
 *
 */

"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	COLORS = require('colors'),
	REDUXCLUSTER = require('redux-cluster'),
	OS = require('os'),
	PATH = require('path'),
	FS = require('fs');
	
//подгружаемые модули
var SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
	SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;

//глобальные переменные
var stdout = new Object;
Object.assign(stdout, console);

var dateStart = (new Date()).toJSON().substr(0, 10).replace(/[-]/gi, '');

var loggerStorage = REDUXCLUSTER.createStore(loggerStore);
	loggerStorage.stderr = function(){};

function loggerStore(){
	try {
		let state_new = {type:arguments[1].type, text: arguments[1].payload};
		return state_new;
	} catch(e){
		stdout.warn(COLORS.yellow(datetime()+e));
	}
}
function dateFormatter(dt){
	try{
		let y = dt.getFullYear().toString();
		let m = (dt.getMonth() + 1).toString();
		if(m.length < 2) { m = '0'+m; }
		let d = dt.getDate().toString();
		if(d.length < 2) { d = '0'+d; }
		return (y+m+d);
	} catch(err){
		console.log(err);
		return '00000000';
	}
}
//создаю клиент-сервер
if(process.mainModule.filename.indexOf('process.master.js') !== -1){
	FS.unlink(PATH.join(OS.tmpdir(), SERVICENAME+"-log.sock"), () => {
		loggerStorage.createServer({path:PATH.join(__dirname, SERVICENAME+"-log.sock"), logins:{"iretail":"santaclaus"}});
	});
} else {
	loggerStorage.createClient({path:PATH.join(__dirname, SERVICENAME+"-log.sock"), login:"iretail", password:"santaclaus"});
}
//настраиваю клиент-сервер
if(process.mainModule.filename.indexOf('process.console.js') !== -1){	//консоль
	if(process.argv.indexOf("-v") !== -1)
		loggerStorage.subscribe(function(){
			let _strObj = loggerStorage.getState();
			switch(_strObj.type){
				case 'LOG':
					stdout.log(COLORS.green(_strObj.text));
					break;
				case 'WARN':
					stdout.warn(COLORS.yellow(_strObj.text));
					break;
				case 'ERROR':
					stdout.error(COLORS.red(_strObj.text));
					break;
				case 'HTTP':
					stdout.log(COLORS.gray(_strObj.text));
					break;
				case 'DEBUG':
					stdout.log(_strObj.text);
					break;
			}
		});
} else if(process.mainModule.filename.indexOf('process.worker.sub.js') !== -1){		//саб-воркеры, логгирую в файл лога
	try{
		let ToFileLogger = function(service){
			let self = this;
			self.date = dateFormatter(new Date());		//дата запуска
			self.service = service;		//имя службы
			self.flags = {		//флаг инициализации сокета
				error: false,
				warning: false,
				log: false
			};
			self.paths = {		//пути к файлам логов
				error: PATH.join(__dirname, 'logs', self.date+'_'+self.service+'-error.log'),
				warning: PATH.join(__dirname, 'logs', self.date+'_'+self.service+'-warning.log'),
				log: PATH.join(__dirname, 'logs', self.date+'_'+self.service+'.log')
			}
			self.sockets = {	//сокеты
				error: FS.createWriteStream(self.paths.error, {flags:"a+"}),
				warning: FS.createWriteStream(self.paths.warning, {flags:"a+"}),
				log: FS.createWriteStream(self.paths.log, {flags:"a+"})
			}
			self.logfunc = {	//функции логгирования
				error: function(data){ if(self.flags.error) { self.sockets.error.write(data); } },
				warning: function(data){ if(self.flags.warning) { self.sockets.warning.write(data); } },
				log: function(data){ if(self.flags.log) { self.sockets.log.write(data); } }
			}
			self.onopen = function(socket, type){	//контроль потоков
				switch(type){
					case 'error':
						socket.on('error', function(){});
						socket.on('ready', function(){ 
							self.flags.error = true; 
							console.log('ERROR:   ' + self.paths.error);
							self.logfunc.error("\r\n");
						});
						socket.on('close', function(){ 
							self.flags.error = false;
							self.paths.error = PATH.join(__dirname, 'logs', self.date+'_'+self.service+'-error.log');
							self.sockets.error = FS.createWriteStream(self.paths.error, {flags:"a+"});
							self.onopen(self.sockets.error, 'error');
						});
						break;
					case 'warning':
						socket.on('error', function(err){ stdout.error('LOGGER-> Ошибка потока записи: '+err); });
						socket.on('ready', function(){ 
							self.flags.warning = true; 
							console.log('WARNING: ' + self.paths.warning);
							self.logfunc.warning("\r\n");
						});
						socket.on('close', function(){ 
							self.flags.warning = false;
							self.paths.warning = PATH.join(__dirname, 'logs', self.date+'_'+self.service+'-warning.log');
							self.sockets.warning = FS.createWriteStream(self.paths.warning, {flags:"a+"});
							self.onopen(self.sockets.warning, 'warning');
						});
						break;
					case 'log':
						socket.on('error', function(err){ stdout.warn('LOGGER-> Ошибка потока записи: '+err); });
						socket.on('ready', function(){ 
							self.flags.log = true; 
							console.log('LOG:     ' + self.paths.log);
							self.logfunc.log("\r\n");
						});
						socket.on('close', function(){
							self.flags.log = false;
							self.paths.log = PATH.join(__dirname, 'logs', self.date+'_'+self.service+'.log');
							self.sockets.log = FS.createWriteStream(self.paths.log, {flags:"a+"});
							self.onopen(self.sockets.log, 'log');
						});
						break;
				}
			}
			self.unsubscriber = loggerStorage.subscribe(function(){
				let _strObj = loggerStorage.getState();
				switch(_strObj.type){
					case 'ERROR':
						self.logfunc.error('ERROR: '+_strObj.text+'\r\n');
						break;
					case 'WARN':
						self.logfunc.warning('WARN: '+_strObj.text+'\r\n');
						break;
				}
				switch(_strObj.type){
					case 'LOG':
						self.logfunc.log('INFO:  '+_strObj.text+'\r\n');
						break;
					case 'WARN':
						self.logfunc.log('WARN:  '+_strObj.text+'\r\n');
						break;
					case 'ERROR':
						self.logfunc.log('ERROR: '+_strObj.text+'\r\n');
						break;
					case 'HTTP':
						self.logfunc.log('HTTP:  '+_strObj.text+'\r\n');
						break;
					case 'DEBUG':
						self.logfunc.log('DEBUG: '+_strObj.text+'\r\n');
						break;
				}
			});
			self.onopen(self.sockets.error, 'error');
			self.onopen(self.sockets.warning, 'warning');
			self.onopen(self.sockets.log, 'log');
			self.rotatelog = function(){
				let _date = dateFormatter(new Date());
				if(self.date !== _date){
					self.date = _date;
					self.sockets.error.end();
					self.sockets.warning.end();
					self.sockets.log.end();
				}
			}
			self.rotationcancel = setInterval(self.rotatelog, 60000);
			return self;
		}
		new ToFileLogger(SERVICENAME);
	} catch(err){
		console.log(err);
	}
} else if(process.mainModule.filename.indexOf('process.worker.task.js') !== -1){		//таск-воркеры, отписываюсь от обновлений
	loggerStorage.role.splice(loggerStorage.role.indexOf("worker"), 1);	
} else if(process.mainModule.filename.indexOf('process.master.js') !== -1){		//мастер, превращаю dispatch в асинхронный (чтобы основной procstore не тормозил из-за одинаковых приоритетов)
	loggerStorage.dispatchNEWASYNC = loggerStorage.dispatchNEW;
	loggerStorage.dispatchNEW = function(_v){
		setTimeout(loggerStorage.dispatchNEWASYNC, 0, _v);
	}
}

//функция метки времени
function datetime() {
	try {
		let dataObject = new Date;
		let resultString;
		if(dataObject.getDate() > 9){
			resultString = dataObject.getDate() + '.';
		} else {
			resultString = '0' + dataObject.getDate() + '.';
		}
		if((dataObject.getMonth()+1) > 9){
			resultString = resultString + (dataObject.getMonth()+1) + '.' + dataObject.getFullYear() + ' ';
		} else {
			resultString = resultString + '0' + (dataObject.getMonth()+1) + '.' + dataObject.getFullYear() + ' ';
		}
		if(dataObject.getHours() > 9){
			resultString = resultString + dataObject.getHours() + ':';
		} else {
			resultString = resultString + '0' + dataObject.getHours() + ':';
		}
		if(dataObject.getMinutes() > 9){
			resultString = resultString + dataObject.getMinutes() + ':';
		} else {
			resultString = resultString + '0' + dataObject.getMinutes() + ':';
		}
		if(dataObject.getSeconds() > 9){
			resultString = resultString + dataObject.getSeconds();
		} else {
			resultString = resultString + '0' + dataObject.getSeconds();
		}
		return resultString + " | ";
	} catch(e){
		return '00.00.0000 00:00:00 | ';
	}
}

//ошибки
function error(_val){
	let val = datetime() + _val;
	setTimeout(function(){
		if(process.mainModule.filename.indexOf('process.console.js') === -1){
			SENDMAIL.send({message:val, theme:_val.toString().split(" ").slice(0, 10).join(" ")}, stdout);
			loggerStorage.dispatch({type:"ERROR" ,payload:val});
		} else {
			stdout.error(COLORS.red(val));
		}
	}, 0);
}

//предупреждения
function warn(_val){
	let val = datetime() + _val;
	setTimeout(function(){
		if(process.mainModule.filename.indexOf('process.console.js') === -1){
			loggerStorage.dispatch({type:"WARN" ,payload:val});
		}else {
			stdout.warn(COLORS.yellow(val));
		}
	}, 0);
}

//логи
function log(_val){
	let val = datetime() + _val;
	setTimeout(function(){
		if(process.mainModule.filename.indexOf('process.console.js') === -1){
			loggerStorage.dispatch({type:"LOG" ,payload:val});
		} else {
			stdout.log(COLORS.green(val));
		}
	}, 0);
}

//http-запросы
function http(_val){
	let val = datetime() + _val;
	setTimeout(function(){
		if(process.mainModule.filename.indexOf('process.console.js') === -1){
			loggerStorage.dispatch({type: "HTTP",payload: val});
		}else {
			stdout.log(COLORS.gray(val));
		}
	}, 0);
}

//дебаг (object mode)
function debug(_val){
	if(CONFIG.debug){
		setTimeout(function(){
			if(process.mainModule.filename.indexOf('process.console.js') === -1){
				FS.writeFileSync(PATH.join(__dirname, 'logs', dateStart+'_'+SERVICENAME+'-PID'+process.pid.toString()+'.log'), datetime()+_val+'\r\n', {flag: "a"});
				loggerStorage.dispatch({type: "DEBUG",payload: process.pid+' | '+datetime()+_val});
			} else {
				stdout.debug(_val);
			}
		}, 0);
	}
}

module.exports.error = error;
module.exports.warn = warn;
module.exports.log = log;
module.exports.http = http;
module.exports.debug = debug;
module.exports.datetime = datetime;