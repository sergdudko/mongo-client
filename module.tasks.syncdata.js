/**
 *	iRetailCloudClient
 *	(c) 2019 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE SYNCDATA
 *	Синхронизация данных между API-серверами
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));
	
function syncdata(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK SYNCDATA START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = PROCSTORE.getState().names[_uid];
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		let linkfrom, linkto;
		if(task.sync.type === 'to'){
			linkfrom = CONFIG.server.url+'/v3/query'; //ссылка получения данных
			linkto = task.sync.remote+'/v3/data'; //ссылка отправки данных
		} else {
			linkfrom = task.sync.remote+'/v3/query'; //ссылка получения данных
			linkto = CONFIG.server.url+'/v3/data'; //ссылка отправки данных
		}
		/* -> конфигурация */
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK SYNCDATA DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let auth;
		(new Promise(function(resolve, reject){
			if(CONFIG.usedb === false){
				resolve();
			} else {
				MSSQL.getconfig().then(resolve).catch(reject);
			}
		})).then(function(val){
			try{
				if(val && val.login && val.password){
					auth = val.login + ':' + val.password;
				}
				let oneStepTASK = function(request, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
					if(iteration_bad === 0)
						step++;
					if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение (на 5 проходах были проблемы из-за статуса 206 - выполнялся долше 5 минут, увеличение же таймаута запроса затягивает выполнение)
						let _request = LODASH.clone(request);
						(new Promise(function(resolve, reject){
							if(CONFIG.usedb === false){
								let val_select = [];
								if(typeof(PROCSTORE.getState().variable[task.version]) === 'string'){
									val_select.push({ParamValue:{value:PROCSTORE.getState().variable[task.version]}})
								}
								resolve(val_select);
							} else {
								MSSQL.req("SELECT TOP 1 [ParamValue] FROM [dbo].[T_SystemInfo] WHERE [ParamName] = '"+task.version+"'").then(resolve).catch(reject);
							}
						})).then(function(val_select){ 	//версию
							if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
								if(typeof(_request.find["version"]) !== 'object'){
									_request.find["version"] = {};
								}
								if(typeof(val_select[0]['ParamValue'].value) === 'string')
									_request.find["version"]["$gt"] = val_select[0]['ParamValue'].value;
							}
							let headers = {"ics-cache-timeout":task.cache};	//задаю хидеры
							let _restsettings = {
								url:linkfrom,
								method:"POST",
								data:_request,
								datatype:"object",
								timeout: 300000,
								headers:headers
							};
							if((typeof(auth) === 'string') && (task.sync.type === 'to'))
								_restsettings.auth = auth;
							FUNCTIONS.rest(_restsettings).then(function(req_data){	//выполняю POST запрос на сервер (упрощенный вариант, без 208 статуса и хидеров запроса)
								if(parseInt(parseInt(req_data[0])/100) === 2){
									if((req_data[0] === 200) || (req_data[0] === 201)){
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]);
									} else {
										let _str;
										try {
											if(typeof(req_data[1]) === 'object'){
												_str = JSON.stringify(req_data[1]);
											} else {
												_str = req_data[1];
											}
										} catch(_e){
											logger.warn(_e);
										}
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
									}
								}else{
									let _str;
									try {
										if(typeof(req_data[1]) === 'object'){
											_str = JSON.stringify(req_data[1]);
										} else {
											_str = req_data[1];
										}
									} catch(_e){
										logger.warn(_e);
									}
									logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
								}
								switch(req_data[0]){
									case 200:
									case 201:
										(function(){
											let lastversion;
											for(let i = 0; i < req_data[1].length; i++){
												if(typeof(req_data[1][i]) === 'object'){
													lastversion = req_data[1][i].version;
													delete req_data[1][i]._id;
													delete req_data[1][i].version;
												}
											}
											if(req_data[1].length > 0){
												let _restsettings2 = {
													url:linkto,
													method:"POST",
													data:req_data[1],
													datatype:"object",
													timeout: 300000,
													headers:{}
												};
												if((typeof(auth) === 'string') && (task.sync.type === 'from'))
													_restsettings2.auth = auth;
												let oneStepTASK2 = function(_rsettings, iteration_bad2){
													if(iteration_bad2 < 10){
														FUNCTIONS.rest(_rsettings).then(function(req_data){	//выполняю POST запрос на сервер (упрощенный вариант, без 208 статуса и хидеров запроса)
															if(parseInt(parseInt(req_data[0])/100) === 2){
																if((req_data[0] === 200) || (req_data[0] === 201)){
																	logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]);
																} else {
																	let _str;
																	try {
																		if(typeof(req_data[1]) === 'object'){
																			_str = JSON.stringify(req_data[1]);
																		} else {
																			_str = req_data[1];
																		}
																	} catch(_e){
																		logger.warn(_e);
																	}
																	logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
																}
															}else{
																let _str;
																try {
																	if(typeof(req_data[1]) === 'object'){
																		_str = JSON.stringify(req_data[1]);
																	} else {
																		_str = req_data[1];
																	}
																} catch(_e){
																	logger.warn(_e);
																}
																logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
															}
															switch(req_data[0]){
																case 200:
																	if((typeof(req_data[1]) === 'object') && (req_data[1].status === 'ok')){
																		if(CONFIG.usedb === false){
																			let _t = {};
																			_t[task.version] = lastversion;
																			PROCSTORE.dispatch({type:'UPDATE_VARIABLE', payload: _t});
																			setTimeout(oneStepTASK, 1000, _request, 0);
																		} else {
																			MSSQL.version(task.version, lastversion).catch(function(err){ 
																				logger.error(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка обновления версии: '+ err); 
																			}).finally(function(){
																				setTimeout(oneStepTASK, 1000, _request, 0);
																			});
																		}
																	} else {
																		setTimeout(oneStepTASK2, 10000, _rsettings, ++iteration_bad2);
																	}
																	break;
																default:
																	setTimeout(oneStepTASK2, 10000, _rsettings, ++iteration_bad2);
																	break;
															}
														}).catch(function(err){		//ошибка в функции POST запроса
															logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+err);
															setTimeout(oneStepTASK2, 10000, _rsettings, ++iteration_bad);
														});
													} else {
														setTimeout(oneStepTASK, 15000, _request, ++iteration_bad);
													}
												}
												oneStepTASK2(_restsettings2, 0);
											} else {
												setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
											}
										})();
										break;
									case 204:	//данных нет, задача завершена
										(function(){
											exit(true);
										})();
										break;
									case 206:	//данных нет
										(function(){
											setTimeout(oneStepTASK, 15000, _request, ++iteration_bad); //запустим через 1 минуту, но добавим флаг не успешного выполнения чтобы исключить вероятность зависания
										})();
										break;
									default:
										(function(){
											setTimeout(oneStepTASK, 15000, _request, ++iteration_bad); 
										})();
										break;
								}
							}).catch(function(error){	//ошибка в функции POST запроса
								logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+error);
								setTimeout(oneStepTASK, 15000, _request, ++iteration_bad);
							});
						}).catch(function(err){
							rej("Не могу прочитать версию для "+task.version+": "+err);
							logger.debug('TASK SYNCDATA DONE');
						});
					} else {
						exit();
					}
				}
				const req = ({"find": task.request, "limit": task.limit, "skip":0, "sort":{"version": 1}, "requires": []});	//первый запуск сортировка по версии (это важно)
				setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), req, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
			} catch(err){	//ошибка глобальной обертки функции
				rej(err);
				logger.debug('TASK SYNCDATA DONE');
			}
		}).catch(function(err){
			logger.warn(err);
			exit();
		});
	});
}

module.exports = syncdata;