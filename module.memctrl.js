/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  MEMCTRL
 *	Контроль расхода RAM
 *
 */

"use strict"

//подгружаемые библиотеки
var FS = require('fs'),
	OS = require('os'),
	CLUSTER = require('cluster'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;
	
var _filepath = PATH.join("logs", SERVICENAME+'-memctrl.log');

var _memoryLeak = 0;

function memoryControl(){
	let _status;
	if(!CLUSTER.isMaster){
		if((typeof(CLUSTER.worker) === 'object') && (typeof(CLUSTER.worker.id) === 'number')){
			_status = PROCSTORE.getState().worker[CLUSTER.worker.id];
		}
	}
	if((CLUSTER.isMaster) || (_status === 'idle') || (process.mainModule.filename.indexOf('process.worker.sub.js') !== -1)){	//мастер или воркер не выполняющий задач
		let _freeMemory = OS.freemem(); 	//остаток свободной памяти в OS
		let _memoryObj = process.memoryUsage();
		let _requireMem = _memoryObj.heapTotal + _memoryObj.external; 	//размер выделенный куче ( heapUsed всегда меньше ) + кэши
		let _unused = _memoryObj.rss - _requireMem;		//память, которая выделена процессу, но не используется
		let _freeMemProc = parseInt((_freeMemory*100/OS.totalmem()), 10);
		if(CLUSTER.isMaster){
			if(((_freeMemProc < 5) && (_freeMemory < 104857600) && (_unused > 104857600)) || (_unused > 209715200)){	//мастер, осталось меньше 5% RAM, и меньше 100МБ RAM, и невостребованной памяти больше 100МБ или невостребованной памяти больше 200МБ
				let _str = "MASTER-> Обнаружена выделенная, но невостребованая память: "+parseInt((_unused/(1024*1024)), 10)+"MB, всего занято процессом: "+parseInt((_memoryObj.rss/(1024*1024)), 10)+"MB\r\n";
				LOGGER.warn(_str);
				_str = new Date().toJSON() + " | " + _str;
				FS.writeFileSync(_filepath, _str, {flag: "a"});
				_memoryLeak++;
				if(_memoryLeak >= 10){	//течет 10 проходов подряд, убиваю процесс
					setTimeout(process.exit, 1000, 1);
				}
			}
		} else if(((_freeMemProc < 10) && (_freeMemory < 524288000) && (_unused > 104857600)) || (_unused > 209715200)){	//воркер, осталось меньше 10% RAM, и меньше 500МБ RAM, и невостребованной памяти больше 100МБ или невостребованной памяти больше 200МБ
			let _str = "WORKER-> Обнаружена выделенная, но невостребованая память: "+parseInt((_unused/(1024*1024)), 10)+"MB, всего занято процессом: "+parseInt((_memoryObj.rss/(1024*1024)), 10)+"MB\r\n";
			LOGGER.warn(_str);
			_str = new Date().toJSON() + " | " + _str;
			FS.writeFileSync(_filepath, _str, {flag: "a"});
			_memoryLeak++;
			if(_memoryLeak >= 10){	//течет 10 проходов подряд, убиваю процесс
				setTimeout(process.exit, 1000, 1);
			}
		} else {
			_memoryLeak = 0;
		}
	}
}

function startControl(){
	setInterval(memoryControl, 60000);
}
	
module.exports = startControl;