/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	SETTINGS
 *  PARAMETRS
 *	Параметры (отдельным модулем, чтобы не зацикливалось)
 *
 */

"use strict"

//подгружаемые библиотеки
var OS = require('os');
	
var servicename = "iRetailCC";
var myenv = "production";
if(typeof(process.env["NODE_ENV"]) === 'string'){
	myenv = process.env["NODE_ENV"];
}
if(["production", "development"].indexOf(myenv) === -1){
	servicename = servicename+"_"+myenv;
}

var servicerealname = servicename.toLowerCase();
if(OS.platform() === 'win32'){
	servicerealname = servicerealname + '.exe';
}

module.exports.servicename = servicename;
module.exports.myenv = myenv;
module.exports.servicerealname = servicerealname;