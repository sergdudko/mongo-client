/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE SYNCHRONIZATION
 *	Обработка синхронизации справочников
 *
 */
 
"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MSSQL = require(PATH.join(__dirname, 'module.mssql.js'));
	
function synchronization(uid){
	let thistasklog = '\r\n\r\nЛОГ ЗАДАЧИ:\r\n';
	let logger = {};
	for(const key in LOGGER){
		logger[key] = function(val){
			thistasklog = thistasklog+val+'\r\n';
			LOGGER[key](val);
		}
	}
	logger.debug('TASK SYNCHRONIZATION START');
	return new Promise(function(res, rej){
		let step = 0;
		const _uid = uid;
		const taskname = PROCSTORE.getState().names[_uid];
		if(CONFIG.usedb === false)
			throw new Error('В конфиге отключено использование базы данных, параметр usedb (задача '+taskname+' не запущена)!');
		const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		const link = CONFIG.server.url+'/v3/query'; //ссылка получения данных
		const summCount = {summ:0}; //передадим ссылку на объект для подсчета результатов
		const exit = function(flag){	//отправка мастеру ответа, что задание завершено
			logger.debug('TASK SYNCHRONIZATION DONE');
			if(flag){
				res(_uid);
				thistasklog = null;
			}else{
				rej("Выполнение задачи "+taskname+" прекращено на шаге "+step+" из-за ошибок"+thistasklog);
				thistasklog = null;
			}
		};
		let auth;
		MSSQL.getconfig().then(function(val){
			try{
				if(val && val.login && val.password){
					auth = val.login + ':' + val.password;
				}
				let oneStepTASK = function(request, iteration_bad){	//1 шаг задания, нужно чтобы Promise мог запускать сам себя
					if(iteration_bad === 0)
						step++;
					if(iteration_bad < 30){	//если проходов с ошибками больше 30, то прекращаем выполнение (на 5 проходах были проблемы из-за статуса 206 - выполнялся долше 5 минут, увеличение же таймаута запроса затягивает выполнение)
						let _request = LODASH.clone(request);
						let req_hash = FUNCTIONS.hasher(JSON.stringify(_request.find) + _request.limit.toString() + JSON.stringify(_request.sort) + _request.skip.toString() + JSON.stringify(_request.requires));	//собираем хэш запроса (такой же, как в сервере)
						let result_hash;
						let LastUid = '';
						MSSQL.req("SELECT TOP 1 [Hash],[HashResult],[LastUid] FROM [dbo].[ICS_Hash] WHERE [Hash] = '"+req_hash+"' AND [TaskName] = '"+taskname+"'").then(function(val_select){ 	//получаю хэш результата запроса
							if((typeof(val_select) === 'object') && (!LODASH.isEqual(val_select, []))){
								result_hash = val_select[0]['HashResult'].value;
								LastUid = val_select[0]['LastUid'].value;
							} else {
								result_hash = "";
							}
						}).catch(function(){
							result_hash = "";
						}).finally(function(){
							let headers = {"ics-cache-hash":req_hash,"ics-cache-hashresult":result_hash,"ics-cache-timeout":task.cache};	//задаю хидеры
							let _restsettings = {
								url:link,
								method:"POST",
								data:_request,
								datatype:"object",
								headers:headers
							};
							if(typeof(auth) === 'string')
								_restsettings.auth = auth;
							FUNCTIONS.rest(_restsettings).then(function(req_data){	//выполняю POST запрос на сервер
								if(parseInt(parseInt(req_data[0])/100) === 2){
									if((req_data[0] === 200) || (req_data[0] === 201)){
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]);
									} else {
										let _str;
										try {
											if(typeof(req_data[1]) === 'object'){
												_str = JSON.stringify(req_data[1]);
											} else {
												_str = req_data[1];
											}
										} catch(_e){
											logger.warn(_e);
										}
										logger.log(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
									}
								}else{
									let _str;
									try {
										if(typeof(req_data[1]) === 'object'){
											_str = JSON.stringify(req_data[1]);
										} else {
											_str = req_data[1];
										}
									} catch(_e){
										logger.warn(_e);
									}
									logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Результат rest-запроса: "+req_data[0]+" | "+_str);
								}
								switch(req_data[0]){
									case 200:
									case 201:
										(function(){
											let sqlData = FUNCTIONS.tdata({	//подготавливаю результат к заливке в sql
												data: req_data[1], 
												table: task.table, 
												hash: req_data[2]
											});
											if(typeof(sqlData) === 'object'){	//результат заливки готов
												MSSQL.req("DELETE FROM "+task.table.name+" WHERE [ics_hash] = '"+req_data[2]+"'").then(function(){	//очищаю предыдущие данные
													MSSQL.bulk(task, sqlData, summCount).then(function(){	//заливка данных в sql
														let request_update;
														if((typeof(req_data[1]) === 'object') && (typeof(req_data[1].length) !== 'undefined')){ //получим последний uid из выборки объектов
															if(typeof(req_data[1][req_data[1].length - 1]) === 'object'){
																LastUid = req_data[1][req_data[1].length - 1]._id;
															}
														}
														if(headers["ics-cache-hashresult"] !== ""){	//если смогли получить ics-cache-hashresult из базы, значит запись существует
															request_update =  "UPDATE [dbo].[ICS_Hash] SET [HashResult] = '"+req_data[3]+"', [TaskName] = '"+taskname+"',[LastUid] = '"+LastUid+"' WHERE [Hash] = '"+req_data[2]+"' AND [TaskName] = '"+taskname+"'";
														} else {	//если не смогли получить ics-cache-hashresult из базы - добавим запись
															request_update =  "INSERT INTO [dbo].[ICS_Hash] ([Hash], [HashResult], [TaskName], [LastUid]) VALUES ('"+req_data[2]+"','"+req_data[3]+"','"+taskname+"','"+LastUid+"')";
														}
														MSSQL.req(request_update).then(function(){	//запрос на обновления пары хэшей
															if(typeof(_request.find["_id"]) !== 'object'){
																_request.find["_id"] = {};
															}
															_request.find["_id"]["$gt"] = LastUid;
															setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, 0);	//запуск самой себя (со сбросом счетчика итераций)
														}).catch(function(err){
															logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка работы с MSSQL.req: "+err);
															setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
														});
													}).catch(function(error){	//ошибка в функции заливки данных в sql
														logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка работы с MSSQL.bulk: "+error);
														setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
													});										
												}).catch(function(err){
													logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка работы с MSSQL.req: "+err);
													setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
												});								
											} else {	//не смог подготовить результат к заливке в sql
												logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка преобразования входящего json: " + sqlData);
												setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
											}
										})();
										break;
									case 204:	//данных нет
										(function(){
											MSSQL.req("DELETE FROM "+task.table.name+" WHERE [ics_hash] = '"+req_hash+"'").then(function(){	//очищаю данные (запрос вернулся пустым)
												let request_update;
												if(headers["ics-cache-hashresult"] !== ""){	//если смогли получить ics-cache-hashresult из базы, значит запись существует
													request_update =  "UPDATE [dbo].[ICS_Hash] SET [HashResult] = '"+FUNCTIONS.hasher("")+"', [TaskName] = '"+taskname+"' WHERE [Hash] = '"+req_hash+"' AND [TaskName] = '"+taskname+"'";  
												} else {	//если не смогли получить ics-cache-hashresult из базы - добавим запись
													request_update =  "INSERT INTO [dbo].[ICS_Hash] ([Hash], [HashResult], [TaskName]) VALUES ('"+req_hash+"','"+FUNCTIONS.hasher("")+"','"+taskname+"')";
												}
												MSSQL.req(request_update).then(function(){	//обновляю хэш запроса
													exit(true);
												}).catch(function(err){
													logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка работы с MSSQL.req: "+err);
													setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
												});											
											}).catch(function(err){
												logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+"| Ошибка работы с MSSQL.req: "+err);
												setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, ++iteration_bad);
											});
										})();
										break;
									case 206:	//данных нет
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); //запустим через 1 минуту, но добавим флаг не успешного выполнения чтобы исключить вероятность зависания
										})();
										break;
									case 208:	//данные не изменились, запускаемся со сдвигом
										(function(){
											if(typeof(_request.find["_id"]) !== 'object'){
												_request.find["_id"] = {};
											}
											_request.find["_id"]["$gt"] = LastUid;
											setTimeout(oneStepTASK, FUNCTIONS.randomgen(3000), _request, 0);	//запуск самой себя (со сбросом счетчика итераций)
										})();
										break;
									default:
										(function(){
											setTimeout(oneStepTASK, 60000, _request, ++iteration_bad); 
										})();
										break;
								}
							}).catch(function(error){	//ошибка в функции POST запроса
								logger.warn(FUNCTIONS.correcterstr(taskname, 50)+FUNCTIONS.correcterstr("| шаг "+step, 10)+'| Ошибка Rest-запроса: '+error);
								setTimeout(oneStepTASK, 60000, _request, ++iteration_bad);
							});
						});
					} else {
						exit();
					}
				}
				const req = ({"find": task.request, "limit": task.limit, "skip":0, "sort":{"_id": 1}, "requires":[]});	//первый запуск, сортировка по _id это важно
				setTimeout(oneStepTASK, FUNCTIONS.randomgen(10000), req, 0);	//чтоб не DDoS-ить запускаем запросы с разбросом в 10 сек и далее с разбросом в 1 сек
			} catch(err){	//ошибка глобальной обертки функции
				rej(err);
				logger.debug('TASK SYNCHRONIZATION DONE');
			} 
		}).catch(function(err){
			logger.warn(err);
			exit();
		});
	});
}

module.exports = synchronization;