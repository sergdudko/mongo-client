/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASKS
 *	Экспорт всех типов задач в модуль
 *
 */
 
"use strict"

 var PATH = require('path');

//подгружаемые модули
var SYNCHRONIZATION = require(PATH.join(__dirname, 'module.tasks.synchronization.js')),
	UPSERT = require(PATH.join(__dirname, 'module.tasks.upsert.js')),
	DELINS = require(PATH.join(__dirname, 'module.tasks.delins.js')),
	UPDATE = require(PATH.join(__dirname, 'module.tasks.update.js')),
	TRANSACTION = require(PATH.join(__dirname, 'module.tasks.transaction.js')),
	SENDDATA = require(PATH.join(__dirname, 'module.tasks.senddata.js')),
	UPDFROMGIT = require(PATH.join(__dirname, 'module.tasks.updfromgit.js')),
	PUSHTOGIT = require(PATH.join(__dirname, 'module.tasks.pushtogit.js')),
	UNLOADPROCEDURE = require(PATH.join(__dirname, 'module.tasks.unloadprocedure.js')),
	DOWNLOADPROCEDURE = require(PATH.join(__dirname, 'module.tasks.downloadprocedure.js')),
	SYNCDATA = require(PATH.join(__dirname, 'module.tasks.syncdata.js')),
	DBVERSION = require(PATH.join(__dirname, 'module.tasks.dbversion.js'));
	
module.exports.synchronization = SYNCHRONIZATION;
module.exports.upsert = UPSERT;
module.exports.delins = DELINS;
module.exports.update = UPDATE;
module.exports.transaction = TRANSACTION;
module.exports.senddata = SENDDATA;
module.exports.updfromgit = UPDFROMGIT;
module.exports.pushtogit = PUSHTOGIT;
module.exports.unloadprocedure = UNLOADPROCEDURE;
module.exports.downloadprocedure = DOWNLOADPROCEDURE;
module.exports.syncdata = SYNCDATA;
module.exports.dbversion = DBVERSION;