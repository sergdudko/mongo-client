/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  TASK TYPE PUSHTOGIT
 *	Обновление репозитория git из папки
 *
 */
 
"use strict"

//подгружаемые библиотеки
var LODASH = require('lodash'),
	GIT = require('simple-git/promise'),
	FS = require("fs"), 
	PATH = require("path"),
	CHILDPROCESS = require('child_process'),
	OS = require('os'),
	KILL = require('tree-kill');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js'));
	
function PusherToGit(object){
	/**
	 *	Работает с удаленной веткой origin.
	 *	- repo - сылка на удаленный репозиторий
	 *	- branch - ветка удаленного репозитория
	 * 	- dir - папка для синхронизации, пути включающие слэш [\] должны экранироваться вторым слэшем [\\]
	 **/
	const self = this;
	self.username = "";
	self.password = "";
	self.syncdir = [];
	Object.assign(self, object);

	return new Promise(function(resolve, reject){
		let i = 0;
		let stepOne = function(i, e){
			try{
				let _git;
				let _v = i;
				let _e = e;
				let _temp = self.syncdir[_v].repo.split("://");
				let remote = _temp[0]+"://"+self.username+":"+self.password+"@"+_temp[1];
				(new Promise(function(res, rej){
					FS.stat(self.syncdir[_v].dir, function(err, stats){
						if(typeof(stats) === 'undefined'){
							LOGGER.debug('DIRECTORY '+self.syncdir[_v].dir+' IS UNDEFINED');
							rej(err);
						} else {
							LOGGER.debug('DIRECTORY '+self.syncdir[_v].dir+' IS EXISTS');
							res();
						}
					});
				})).then(function(){
					return GIT(self.syncdir[_v].dir);
				}).then(function(res){
					_git = res;
					LOGGER.debug('SELECT DIRECTORY '+self.syncdir[_v].dir);
					_git.cwd(self.syncdir[_v].dir).then(function(){
						return new Promise(function(res){
							FS.stat(PATH.join(self.syncdir[_v].dir, '.git', 'config'), function(err, stats){
								if(typeof(stats) === 'undefined'){
									res(false);
								} else {
									res(true);
								}
							});
						});	
					}).then(function(_val){
						if(_val === true){
							LOGGER.debug('REPOSITORY IN '+self.syncdir[_v].dir+' IS EXISTS, CHECK REMOTE URL');
							return new Promise(function(res1, rej1){
								let _flg0 = true;
								_git.getRemotes(true).then(function(_remote){
									for(const _id in _remote){
										if(_remote[_id].name === 'origin'){
											_flg0 = false;	//блокирую запуск добавления репозитория
											LOGGER.debug('REMOTE origin IN '+self.syncdir[_v].dir+' IS EXISTS');
											if(_remote[_id].refs.fetch === remote){
												res1(true);
											} else {
												LOGGER.debug('REPLACE REMOTE origin IN '+self.syncdir[_v].dir);
												_git.removeRemote('origin').then(function(){
													return _git.addRemote('origin', remote);
												}).then(function(_val2){
													res1(_val2);
												}).catch(function(_err2){
													rej1(_err2);
												});
											}
										}
									}
									if(_flg0){
										LOGGER.debug('ADD REMOTE origin IN '+self.syncdir[_v].dir);
										_git.addRemote('origin', remote).then(function(_val2){
											res1(_val2);
										}).catch(function(_err2){
											rej1(_err2);
										});
									}
								});
							});
						} else {
							LOGGER.debug('INIT REPOSITORY IN '+self.syncdir[_v].dir);
							return new Promise(function(res1, rej1){
								_git.init().then(function(){
									LOGGER.debug('ADD REMOTE TO REPOSITORY IN '+self.syncdir[_v].dir);
									return _git.addRemote('origin', remote);
								}).then(function(){
									LOGGER.debug('GET CONFIG PARAMETRS');
									return new Promise(function(_res, _rej){
										_git.raw(['config', '--list']).then(function(_cnf){
											return new Promise(function(_res1){
												let _cnfobj = {};
												let _cnf0 = _cnf.split('\n');
												for(const _id in _cnf0){
													if((typeof(_cnf0[_id]) === 'string') && (_cnf0[_id].indexOf("=") !== -1)){
														let _t = _cnf0[_id].split("=");
														if(_t.length >=2){
															_cnfobj[_t[0]] =_t[1];
														}
													}
												}
												_res1(_cnfobj);
											});
										}).then(function(_cnfobj){
											if(typeof(_cnfobj['user.name']) !== 'string'){
												LOGGER.debug('CONFIG PARAMETRS ADD USERNAME');
												return _git.addConfig('user.name', 'IRetail Cloud Client');
											} else {
												return new Promise(function(_res1){ _res1(_cnfobj); });
											}
										}).then(function(_cnfobj){
											if(typeof(_cnfobj['user.email']) !== 'string'){
												LOGGER.debug('CONFIG PARAMETRS ADD USERMAIL');
												return _git.addConfig('user.email', 'ics@farmin.by');
											} else {
												return new Promise(function(_res1){ _res1(_cnfobj); });
											}
										}).then(function(_cnfobj){
											_res(_cnfobj);
										}).catch(_rej);
									});
								}).then(function(_val1){
									res1(_val1);
								}).catch(function(err1){
									rej1(err1);
								});
							});
						}
					}).then(function(){
						LOGGER.debug('ADD FILES FROM '+self.syncdir[_v].dir+' TO BRANCH master');
						return _git.add('--all');
					}).then(function(){
						LOGGER.debug('COMMIT CHANGE IN '+self.syncdir[_v].dir+' TO BRANCH master');
						let _msg = LOGGER.datetime();
						return _git.commit(_msg + 'AutoBackup from '+SENDMAIL.identifier+' ( iRetailCloudClient )', undefined, { '--author': '"iRetail Cloud Client <ics@farmin.by>"' });
					}).then(function(){
						if(self.syncdir[_v].branch !== 'master'){
							LOGGER.debug('SET DATA FROM BRANCH master TO BRANCH '+self.syncdir[_v].branch);
							return _git.raw(['branch', self.syncdir[_v].branch, 'master', '-f']);
						} else {
							return new Promise(function(res1){res1();});
						}
					}).then(function(){
						LOGGER.debug('PUSH DATA FROM BRANCH '+self.syncdir[_v].branch+' TO REMOTE SERVER');
						return _git.push('origin', self.syncdir[_v].branch, {"-f":null});
					}).catch(function(err){
						_e = err;
					}).finally(function(){
						LOGGER.debug('NEXT DIRECTORY');
						let _t = _v+1;
						if(_t < self.syncdir.length){
							stepOne(_t, _e);
						} else {
							if(typeof(_e) !== 'undefined'){
								reject(_e);
							} else {
								resolve(true);
							}
						}
					});
				}).catch(function(err){
					let _t = i+1;
					if(_t < self.syncdir.length){
						stepOne(_t, err);
					} else {
						reject(err);
					}
				});
			} catch(err){
				let _t = i+1;
				if(_t < self.syncdir.length){
					stepOne(_t, err);
				} else {
					reject(err);
				}
			}
		}
		if(typeof(self.syncdir[i]) === 'object') 
			stepOne(i);
		else
			reject(new Error("Не задана директория для синхронизации!"));
	});
	
}
	
function pushtogit(uid){
	LOGGER.debug('TASK PUSHTOGIT START');
	const _uid = uid;
	const taskname = PROCSTORE.getState().names[_uid];
	const task = LODASH.clone(PROCSTORE.getState().tasks[_uid]); //клонирую задание, чтобы оно не изменялось во время выполнения
		return new Promise(function(rs, rj){
		let platform = OS.platform();
		(new Promise(function(rs1, rj1){
			if(
				(typeof(task.shell) === 'object') && 
				(typeof(task.shell[platform]) === 'object') && 
				(typeof(task.shell[platform].pre) === 'object') && 
				(typeof(task.shell[platform].pre.com) === 'string') && 
				Number.isInteger(task.shell[platform].pre.timeout)
			){
				(new Promise(function(res, rej){
					let lasterr, out;
					let buffers_err = [],
						buffers_out = [];
					let childproc = CHILDPROCESS.spawn(task.shell[platform].pre.com, {
						detached: true,
						shell: true
					}).on('error', function(err){
						lasterr = err;
					}).on('close', function(exit){ 
						if((typeof(lasterr) === 'undefined') && (buffers_err.length > 0)){ 
							if(platform === 'win32'){
								lasterr = FUNCTIONS.strDecoder(Buffer.concat(buffers_err)); 
							} else {
								lasterr = Buffer.concat(buffers_err); 
							}
						}
						if(buffers_out.length > 0){
							if(platform === 'win32'){
								out = FUNCTIONS.strDecoder(Buffer.concat(buffers_out)); 
							} else {
								out = Buffer.concat(buffers_out).toString();
							}
						}
						if(out){ LOGGER.debug('EXEC RESULT: '+out); }
						if(lasterr){ LOGGER.debug('EXEC ERRORS: '+lasterr); }
						if((exit === 0)/* && (typeof(lasterr) === 'undefined')*/){
							res(out);
						} else {
							rej(lasterr);
						}
					});
					if(task.shell[platform].pre.timeout !== 0){
						setTimeout(function(){
							lasterr = 'Process killed by timeout!';
							KILL(childproc.pid);
						}, task.shell[platform].pre.timeout*1000);
					}
					childproc.stdout.on('data', function(buffer){
						buffers_out.push(buffer);
					});
					childproc.stderr.on('data', function(buffer){
						buffers_err.push(buffer);
					});
				})).then(rs1).catch(rj1);
			} else {
				(new Promise(function(res){ return res(); })).then(rs1).catch(rj1);
			}
		})).then(function(){
			return new Promise(function(res, rej){
				LOGGER.debug('EXEC GIT START');
				let badstep = 0;
				let _step = function(_b){
					(new PusherToGit(task.git)).then(function(){
						LOGGER.debug('EXEC GIT DONE');
						res(_uid);
					}).catch(function(err){
						_b++;
						let _e;
						if(((typeof(task.git.username) === 'string') && (task.git.username.length > 0)) || ((typeof(task.git.password) === 'string') && (task.git.password.length > 0))){
							_e = new Error(err.toString().replace(FUNCTIONS.regexpstr(task.git.username), "login").replace(FUNCTIONS.regexpstr(task.git.password), "password"));	//удаляю из вывода логин и пароль
						} else {
							_e = err;
						}
						if(_b < 10){
							LOGGER.warn(FUNCTIONS.correcterstr(taskname, 50)+"| ошибка "+_e);
							setTimeout(_step, 15000, _b);
						} else {
							LOGGER.debug('EXEC GIT DONE');
							rej(_e);
						}
					});
				}
				_step(badstep);
			});
		}).then(function(){
			return new Promise(function(rs1, rj1){
				if(
					(typeof(task.shell) === 'object') && 
					(typeof(task.shell[platform]) === 'object') && 
					(typeof(task.shell[platform].post) === 'object') && 
					(typeof(task.shell[platform].post.com) === 'string') && 
					Number.isInteger(task.shell[platform].post.timeout)
				){
					(new Promise(function(res, rej){
						let lasterr, out;
						let buffers_err = [],
							buffers_out = [];
						let childproc = CHILDPROCESS.spawn(task.shell[platform].post.com, {
							detached: true,
							shell: true
						}).on('error', function(err){
							lasterr = err;
						}).on('close', function(exit){ 
							if((typeof(lasterr) === 'undefined') && (buffers_err.length > 0)){ 
								if(platform === 'win32'){
									lasterr = FUNCTIONS.strDecoder(Buffer.concat(buffers_err)); 
								} else {
									lasterr = Buffer.concat(buffers_err); 
								}
							}
							if(buffers_out.length > 0){
								if(platform === 'win32'){
									out = FUNCTIONS.strDecoder(Buffer.concat(buffers_out)); 
								} else {
									out = Buffer.concat(buffers_out).toString();
								}
							}
							if(out){ LOGGER.debug('EXEC RESULT: '+out); }
							if(lasterr){ LOGGER.debug('EXEC ERRORS: '+lasterr); }
							if((exit === 0)/* && (typeof(lasterr) === 'undefined')*/){
								LOGGER.debug('TASK PUSHTOGIT DONE');
								res(out);
							} else {
								LOGGER.debug('TASK PUSHTOGIT DONE');
								rej(lasterr);
							}
						});
						if(task.shell[platform].post.timeout !== 0){
							setTimeout(function(){
								lasterr = 'Process killed by timeout!';
								KILL(childproc.pid);
							}, task.shell[platform].post.timeout*1000);
						}
						childproc.stdout.on('data', function(buffer){
							buffers_out.push(buffer);
						});
						childproc.stderr.on('data', function(buffer){
							buffers_err.push(buffer);
						});
					})).then(rs1).catch(rj1);
				} else {
					(new Promise(function(res){ 
						LOGGER.debug('TASK PUSHTOGIT DONE');
						return res(); 
					})).then(rs1).catch(rj1);
				}
			});
		}).then(rs).catch(rj);
	});
}

module.exports = pushtogit;