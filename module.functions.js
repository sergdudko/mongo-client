/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  FUNCTIONS
 *	Функции приложения
 *
 */

"use strict"

//подгружаемые библиотеки
var CRYPTO = require('crypto'),
	LODASH = require('lodash'),
	HTTP = require('http'),
	HTTPS = require('https'),
	URL = require('url'),
	ZLIB = require('zlib'),
	STREAM = require('stream'),
	MKDIRP = require('mkdirp'),
	CLUSTER = require('cluster'),
	PATH = require('path'),
	ICONV = require('iconv-lite');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js'));

const hrtimeproc = process.hrtime.bigint();
var performance = {
	now:function(){
		try{
			const hrtimeprocnew = process.hrtime.bigint();
			const my = [parseInt(((hrtimeprocnew - hrtimeproc) / 1000000n).toString()), parseInt(((hrtimeprocnew - hrtimeproc) % 1000000n).toString())];
			return parseFloat(my[0]+"."+my[1]);
		} catch(err){
			LOGGER.error(err);
		}
	}
};

//добивает строку пробелами до нужной длинны или обрезает
function correcterString(str, len){
	try{
		let str_t;
		if(typeof(str) !== 'undefined'){
			if(typeof(str) !== 'string'){
				str_t = str.toString();
			} else{
				str_t = str;
			}
		} else {
			str_t = 'undefined';
		}
		if(str_t.length < len){
			for(var i=str_t.length; i < len; i++){
				str_t = str_t + ' ';
			}
		} else {
			str_t = str_t.substr(0, len);
		}
		return str_t;
	} catch(e){
		return str;
	}
}

//функция подсчета хэша
function Hasher(data){
	try{
		const hash = CRYPTO.createHash('sha1');
		hash.update(data);
		return(hash.digest('hex'));
	} catch(e){
		return "error";
	}
}

//подготовка к старту (проверка папок)
function PreStart(dirarr){
	return new Promise(function (resolve, reject){
		var Step = [];
		for(let i =0; i < dirarr.length; i++){
			const _i = i;
			Step.push(new Promise(function (_resolve, _reject){
				let thisdir = PATH.join(__dirname, '/'+dirarr[_i]+'/');
				MKDIRP(thisdir, function (err) {
					if (err){
						_reject('PRESTARTER-> директория '+thisdir+' не создана по причине: '+ err);
					} else {
						_resolve('ok');
					}
				});
			}));
		}
		Promise.all(Step).then(_resolve =>{resolve(_resolve);}, _reject =>{reject(_reject);});
	});
}

//функция генерации UID
function generateUID() { 
	let d = new Date().getTime();
	if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
		d += performance.now(); 
	}
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		let r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
}

//REST-запрос
/* 
Data:{
	url:"url",
	method:"method",
	data:"data"
	datatype:"string/object/null"
	headers:{"key":"value"}	
}

для статусов 
200 и 201 - вернет объект данных (object),
204,206,208 - вернет число соответствующее статусу (number),
остальные - (string), где "error" - ошибка в функции */
function RestRequest(data){
	LOGGER.debug('FUNCTIONS RESTREQUEST START');
	let _data = LODASH.clone(data);
	return new Promise(function(resolve, reject){
		const timeout = 120000;
		let getoptions = URL.parse(_data.url);	//создаем параметры запроса 
		getoptions.method = _data.method;
		getoptions.timeout = timeout;
		getoptions.headers = {};
		getoptions.headers["User-Agent"] = "iRetailCloudClient v"+PROCSTORE.getState().version;
		getoptions.headers["Keep-Alive"] = "120";
		getoptions.headers["Accept-Charset"] = 'utf-8';
		getoptions.headers["Content-type"] = 'application/json';
		getoptions.headers["Accept-Encoding"] = "gzip";
		if(Number.isInteger(_data.timeout)){
			getoptions.timeout = _data.timeout;
		}
		if(typeof(_data.headers) === 'object'){
			for(const key in _data.headers){
				getoptions.headers[key] = _data.headers[key];
			}
		}
		if((typeof(_data.auth) === 'string') && (_data.auth !== '')){	//Basic авторизация
			getoptions.auth = _data.auth;
		}
		let req;
		if (URL.parse(_data.url).protocol === null) {	//определяем тип сервера и используемую библиотеку
			req = HTTP;
		} else if (URL.parse(_data.url).protocol === 'https:') {
			req = HTTPS;
		} else {
			req = HTTP;
		}
		let this_request = req.request(getoptions, (response) => {
			LOGGER.debug('FUNCTIONS RESTREQUEST ANSWR');
			let postdata = [];	//массив буфферов результата запроса
			const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
			const Writable = STREAM.Writable();	//поток записи
			Writable._write = function (chunk, enc, next) {	//обработка потока
				postdata.push(chunk);	//пушим буффер в массив
				next();
			};
			function closerErrStream(this_data){
				gunzipper.close(); //закрываем gunzip (а то плюется unexpected end of file)
				response.unpipe(Writable); //отвязываем потоки	
				response.destroy(); //уничтожаем потоки
				Writable.destroy();
				if(this_data){
					reject('Ошибка обработки потоков: ' + this_data);
					LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
				}
			}
			gunzipper.on("error", function(err){ //обработка ошибок потоков
				closerErrStream(err);
			});
			Writable.on("error", function(err){ 
				closerErrStream(err);
			});
			response.on("error", function(err){ 
				closerErrStream(err);
			});
			Writable.on('finish', () => { 
				gunzipper.close(); //закрываем gunzip (а то плюется unexpected end of file)
				response.unpipe(); //отвязываем потоки
				try{
					resolve([response.statusCode, JSON.parse((Buffer.concat(postdata)).toString('utf8').replace(/[\ufffe\ufeff\uffef\uffbb\uffbf]/g, '')), response.headers['ics-cache-hash'], response.headers['ics-cache-hashresult']]); //херим спецификацию (нечитаемые символы)
					LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
				} catch(err){
					reject('Ошибка преобразования rest-запроса: ' + err);
					LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
				}
			});
			switch(response.statusCode){
				case 200:
					if(response.headers['content-encoding'] === 'gzip'){
						response.pipe(gunzipper).pipe(Writable);
					} else {
						response.pipe(Writable);
					}
					break;
				case 201:	//данные кэшированы
					if(response.headers['content-encoding'] === 'gzip'){
						response.pipe(gunzipper).pipe(Writable);
					} else {
						response.pipe(Writable);
					}
					break;
				default:
					if((typeof(response.headers) === 'object') && (typeof(response.headers['content-type']) === 'string') && (response.headers['content-type'].indexOf("application/json") !== -1)){
						if(response.headers['content-encoding'] === 'gzip'){
							response.pipe(gunzipper).pipe(Writable);
						} else {
							response.pipe(Writable);
						}
					} else {
						resolve([response.statusCode,  response.statusMessage, response.headers['ics-cache-hash'], response.headers['ics-cache-hashresult']]);
						closerErrStream();
						LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
					}
					break;
			}
		});
		const gzipper = ZLIB.createGzip();	//поток сжатия
		if(_data.headers.encoding === 'gzip'){
			gzipper.pipe(this_request);
		}
		if((typeof(_data.datatype) !== 'undefined') && (typeof(_data.data) !== 'undefined')){
			let _tmp;
			switch(_data.datatype){
				case 'object':
					_tmp = JSON.stringify(_data.data);
					LOGGER.debug(_tmp);
					if(_data.headers.encoding === 'gzip'){	//отправка post-данных
						gzipper.write(_tmp);
					} else {
						this_request.write(_tmp);
					}
					break;
				case 'string':
					LOGGER.debug(_data.data);
					if(_data.headers.encoding === 'gzip'){	//отправка post-данных
						gzipper.write(_data.data);
					} else {
						this_request.write(_data.data);
					}
					break;
			}
		}
		this_request.on('error', function (e) {	//обработка ошибок
			reject('Ошибка rest-запроса:'+e);
			LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
		});
		this_request.on('timeout', function () {	//обработка таймаута
			this_request.abort();
			reject('Таймаут rest-запроса!');
			LOGGER.debug('FUNCTIONS RESTREQUEST DONE');
		});
		if(_data.headers.encoding === 'gzip'){
			gzipper.end();
		} else {
			this_request.end();
		}
	});
}

//функция генерации случайного целого с верхним пределом в data
function RandomGen(data){
	try{
		let wid;
		if((typeof(CLUSTER.worker.id) === 'number') && !isNaN(CLUSTER.worker.id)){
			if(CLUSTER.isMaster){
				if(data > 3000){
					wid = 1000;
				} else {
					wid = 0;
				}
			} else if(CLUSTER.isWorker){
				if((data > 10000) && (CLUSTER.worker.id < 10)){
					wid = CLUSTER.worker.id * 1000;
				} else if((data > 1000) && (CLUSTER.worker.id < 10)) {
					wid = CLUSTER.worker.id * 100;
				} else {
					wid = CLUSTER.worker.id;
				}
			} else {
				if(data > 3000){
					wid = 1000;
				} else {
					wid = 0;
				}
			}
		}
		return parseInt(Math.random() * (data - wid) + wid)
	} catch(err){
		return 1000;
	}
}

//преобразование объекта данных для mssql (чистая). аргумент hash означает что метод будет bulk и в нем не будут участвовать forbidden документы
function transformData(object){
	try{
		const _object = LODASH.clone(object);
		if(typeof(_object.table.notnull) !== 'undefined')
			var clearNotnull = LODASH.clone(_object.table.notnull);
		let resultArray = [];	//результирующий массив
		for(const param in _object.data){ //выбираю 1 объект
			if((typeof(_object.hash) === 'undefined') || ((_object.data[param].forbidden !== true) && (_object.data[param].forbidden !== 'true'))){	//проверяю что запрос содержит хэш (bulk, тут forbidden не нужны), тогда проверяем что документ не удален в базе, иначе не обрабатываю
				let RawForm = {};	//временный объект
				for(const margin in _object.table.margins){	//перебираю поля таска
					if(typeof(_object.table.margins[margin]) === 'object'){		//поле таска объект (сопоставление)
						if(typeof(_object.table.margins[margin].param) !== 'undefined'){ 	//параметр
							if(typeof(_object.data[param][_object.table.margins[margin].param]) !== 'string')//приводим все поля к ANSI (если использовать VarChar, а если NVarChar то не надо)
								RawForm[margin] = _object.data[param][_object.table.margins[margin].param];
							else
								RawForm[margin] = _object.data[param][_object.table.margins[margin].param].replace(/'/gi, "''"); //.replace(/\"/gi, '\\\"');
						} else if(typeof(_object.table.margins[margin].requisite) !== 'undefined'){ 	//реквизит
							for(const requisite in _object.data[param].requisites){
								if(_object.table.margins[margin].requisite === _object.data[param].requisites[requisite].id){ //проверяю по id
									if(typeof(_object.data[param].requisites[requisite].value) !== 'string')
										RawForm[margin] = _object.data[param].requisites[requisite].value;
									else
										RawForm[margin] = _object.data[param].requisites[requisite].value.replace(/'/gi, "''"); //.replace(/\"/gi, '\\\"');
								}
							}
						} else if(typeof(_object.table.margins[margin].file) === 'object'){
							let filename = Object.keys(_object.table.margins[margin].file)[0];
							let filereq = _object.table.margins[margin].file[filename];
							if((typeof(filename) === 'string') && (typeof(filereq) === 'string')){
								for(const file in _object.data[param].files){
									if(_object.data[param].files[file].name === filename){
										for(const rfile in _object.data[param].files[file].requisites){
											if(_object.data[param].files[file].requisites[rfile].id === filereq){
												RawForm[margin] = _object.data[param].files[file].requisites[rfile].value;
											}
										}
									}
								}
							}
						}
					} else if(typeof(_object.table.margins[margin]) === 'string') {		//поле таска строка (тупо применяю), переменные могут быть заданы как ***переменная*** и они подставляются при чтении таска
						RawForm[margin] = _object.table.margins[margin].replace(/'/gi, "''"); //.replace(/\"/gi, '\\\"');
					}
				}
				if(typeof(_object.hash) !== 'undefined')
					RawForm['ics_hash'] = _object.hash;
				let RawFormArr = []; //результирующий объект (строка бд)
				if((typeof(_object.table.multiline) === 'undefined') && (typeof(_object.table.array) === 'undefined')) {	//нет array и multiline - просто пушим объект
					RawFormArr.push(LODASH.clone(RawForm));
				}
				if((typeof(_object.table.array) !== 'undefined') && (typeof(_object.data[param].arrays) === 'object') && (Array.isArray(_object.data[param].arrays))){	//создаем по 1 копии документа на каждый элемент массива в arrays
					for(let akey = 0; akey < _object.data[param].arrays.length; akey++){
						const _arr = LODASH.clone(_object.data[param].arrays[akey]);
						if(_arr.id === _object.table.array){
							for(const margin in _object.table.margins){
								if(
									(typeof(_object.table.margins[margin]) === 'object') && 
									(_arr.id === _object.table.margins[margin].array) && 
									(typeof(_arr.values) === 'object') && 
									(Array.isArray(_arr.values))
								){
									for(let i =0; i < _arr.values.length; i++){
										const _i = i;
										let _RawForm = LODASH.clone(RawForm);
										if(typeof(_arr.values[_i]) !== 'string')
											_RawForm[margin] = _arr.values[_i];
										else 
											_RawForm[margin] = _arr.values[_i].replace(/'/gi, "''"); //.replace(/\"/gi, '\\\"');
										RawFormArr.push(LODASH.clone(_RawForm));
									}
								}
							}
						}
					}
				}
				if ((typeof(_object.table.multiline) !== 'undefined') && (typeof(_object.data[param].multilines) !== 'undefined')) {	//создаем по 1 копии документа на каждый line в multilines, добавив данные из requisite в line
					for(const _mkey in _object.data[param].multilines){
						if(_object.data[param].multilines[_mkey].id === _object.table.multiline){
							for(const _lkey in _object.data[param].multilines[_mkey].lines){
								let _RawForm = LODASH.clone(RawForm);
								for(const margin in _object.table.margins){
									if((typeof(_object.table.margins[margin]) === 'object') && (typeof(_object.table.margins[margin].multiline) !== 'undefined')){
										for(const _rkey in _object.data[param].multilines[_mkey].lines[_lkey].requisites){
											if(_object.data[param].multilines[_mkey].lines[_lkey].requisites[_rkey].id === _object.table.margins[margin].multiline){
												let _reqisite = _object.data[param].multilines[_mkey].lines[_lkey].requisites[_rkey].value;
												if(typeof(_reqisite) !== 'string')
													_RawForm[margin] = _reqisite;
												else
													_RawForm[margin] = _reqisite.replace(/'/gi, "''"); //.replace(/\"/gi, '\\\"');
											}
										}
									}
								}
								RawFormArr.push(LODASH.clone(_RawForm));
							}
						}
					}
				}
				for(let i=0; i < RawFormArr.length; i++){
					const _i = i;
					if(LODASH.keys(RawFormArr[_i]).length !== (LODASH.keys(_object.table.margins).length +1)){	//валидация
						let json_error = false;
						if(typeof(_object.table.notnull) !== 'undefined')
							for(const margin in _object.table.margins){
								if(typeof(RawFormArr[_i][margin]) === 'undefined'){
									if(clearNotnull.indexOf(margin) !== -1){	//проверяем notnull значения
										json_error = true;
									}
								}
							}
						if(!json_error){
							resultArray.push(LODASH.clone(RawFormArr[_i]));
						}
					} else {
						resultArray.push(LODASH.clone(RawFormArr[_i]));
					} 
				}
			}
		}
		return resultArray;
	} catch(e){
		return 'Ошибка трансформации входящего JSON: ' + e;
	}
} 

function RegExpString(val){
	let _val;
	try{
		if(typeof(_val) !== 'string')
			_val = val.toString();
		else
			_val = val;
		_val = _val.replace(new RegExp("\\^", "gi"), "\\^");
		_val = _val.replace(new RegExp("\\.", "gi"), "\\.");
		_val = _val.replace(new RegExp("\\$", "gi"), "\\$");
		_val = _val.replace(new RegExp("\\*", "gi"), "\\*");
		_val = _val.replace(new RegExp("\\+", "gi"), "\\+");
		_val = _val.replace(new RegExp("\\?", "gi"), "\\?");
		return new RegExp(_val, "gi");
	} catch(e){
		LOGGER.warn('FUNCTIONS REGEXPSTRING ERROR: '+ e);
	}
}

function DateToJSON(date){
	if(!date)
		date = new Date();
	date.setHours( date.getHours()+(date.getTimezoneOffset()/-60) );
	return date.toJSON();
}

function strToUtf8(value, encoding = 'cp1251'){ 
	try {
		if(value instanceof Buffer){
			let thisval = ICONV.decode(Buffer.from(Buffer.from(ICONV.decode(value, encoding)), 'utf8'), 'utf8');
			return thisval;
		} else {
			throw new Error('Function strToUtf8 requireq value argument as Buffer');
		}
	} catch(e){ 
		LOGGER.warn(e);
		return value.toString();
	}
}

function compareVers(v1, v2){
	if((typeof(v1) !== 'string') 
		|| (typeof(v2) !== 'string')
	){
		return new Error('Переданные аргументы не являются строковыми величинами!');
	}
	const _v1 = v1.match(/^(\d+\.)(\d+\.)(\d+\.)(\d+)/gi);
	const _v2 = v2.match(/^(\d+\.)(\d+\.)(\d+\.)(\d+)/gi);
	if(!(Array.isArray(_v1) && (typeof(_v1[0]) === 'string')) 
		|| !(Array.isArray(_v2) && (typeof(_v2[0]) === 'string'))
	){
		return new Error('Переданные аргументы не являются строковыми версиями!');
	}
	try{
		const _vn1 = _v1[0].split('.').map(function(arg){ return Number.parseInt(arg, 10); });
		const _vn2 = _v2[0].split('.').map(function(arg){ return Number.parseInt(arg, 10); });
		for(let i = 0; i < _vn1.length; i++){
			if(_vn1[i] > _vn2[i])
				return 1;
			if(_vn1[i] < _vn2[i])
				return -1;
		}
		return 0;
	} catch(err){
		return new Error('Ошибка сравнения версий:'+err.message);
	}
}

function taskValidator(_task){
	try{
		/* общие поля для всех тасков */
		if((typeof(_task.description) !== 'string') && (typeof(_task.description) !== 'undefined'))
			throw new Error('Тип поля description должен быть string.');
		if((typeof(_task.author) !== 'string') || (_task.author === '')) 
			throw new Error('Таск должен содержать поле author.');
		if((typeof(_task.type) !== 'string') || (typeof(require(PATH.join(__dirname, 'module.tasks.js'))[_task.type]) === 'undefined'))
			throw new Error('Тип таска не задан или не найден модуль для его исполнения.');
		if((!Number.isInteger(_task.interval)) || (_task.interval < 0))
			throw new Error('Тип поля interval должен быть integer > 0.');
		if(!Array.isArray(_task.silent) && (typeof(_task.silent) !== 'undefined'))
			throw new Error('Тип поля silent должен быть string.');	//данное поле преобразуется в array, но в ошибку кидаем string (т.к. пользователь вводит его как строку)
		switch(_task.type){
			case 'senddata':
				/* таски работы отправки данных (senddata) дополнительные поля не используют */
				break;
			case 'pushtogit':
			case 'updfromgit':	
				/* таски работы с git (pushtogit, updfromgit) */
				if((typeof(_task.git) !== 'object') || (Array.isArray(_task.git))) {
					throw new Error('Тип поля git должен быть object.');
				}else if(typeof(_task.git.username) !== 'string'){
					throw new Error('Тип поля git.username должен быть string (может быть пустым).');
				}else if(typeof(_task.git.password) !== 'string'){
					throw new Error('Тип поля git.username должен быть string (может быть пустым).');
				}else if(!Array.isArray(_task.git.syncdir)){
					throw new Error('Тип поля git.syncdir должен быть array.');
				}else if(_task.git.syncdir.length < 1){
					throw new Error('Тип поля git.syncdir не должен быть пустым массивом.');
				}else{
					for(const dir in _task.git.syncdir){
						if((typeof(_task.git.syncdir[dir]) !== 'object') || (Array.isArray(_task.git.syncdir[dir]))){
							throw new Error('Тип поля git.syncdir[] должен быть object.');
						} else if(typeof(_task.git.syncdir[dir].dir) !== 'string'){
							throw new Error('Тип поля git.syncdir[].dir должен быть string.');
						} else if(typeof(_task.git.syncdir[dir].repo) !== 'string'){
							throw new Error('Тип поля git.syncdir[].repo должен быть string.');
						} else if(typeof(_task.git.syncdir[dir].branch) !== 'string'){
							throw new Error('Тип поля git.syncdir[].branch должен быть string.');
						}
					}
				}
				if(typeof(_task.shell) !== 'undefined'){
					if(typeof(_task.shell) !== 'object'){
						throw new Error('Тип поля shell должен быть object.');
					} else {
						for(const platform in _task.shell){
							if(typeof(_task.shell[platform]) !== 'object'){
								throw new Error('Тип поля shell.'+platform+' должен быть object.');
							} else {
								for(const t in _task.shell[platform]){
									if(typeof(_task.shell[platform][t]) !== 'object'){
										throw new Error('Тип поля shell.'+platform+'.'+t+' должен быть object.');
									} else if(typeof(_task.shell[platform][t].com) !== 'string') {
										throw new Error('Тип поля shell.'+platform+'.'+t+'.com должен быть string.');
									} else if(!Number.isInteger(_task.shell[platform][t].timeout)) {
										throw new Error('Тип поля shell.'+platform+'.'+t+'.timeout должен быть Integer.');
									} else if((t === 'file') && (typeof(_task.shell[platform][t].file) !== 'string')){
										throw new Error('Тип поля shell.'+platform+'.'+t+'.file должен быть string.');
									}
								}
							}
						}
					}
				}
				break;
			case 'unloadprocedure':
				/* таски работы с хранимыми процедурами, выгрузка (unloadprocedure) */
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				break;
			case 'downloadprocedure':
				/* таски работы с хранимыми процедурами, загрузка (downloadprocedure) */
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				if((!Number.isInteger(_task.limit)) || (_task.limit < 1))
					throw new Error('Тип поля limit должен быть integer >= 1.');
				if((!Number.isInteger(_task.cache)) || (_task.cache < 1))
					throw new Error('Тип поля cache должен быть integer >= 1.');
				if((typeof(_task.request) !== 'object') || (Array.isArray(_task.request)))
					throw new Error('Тип поля request должен object.');
				break;
			case 'delins':
			case 'update':
			case 'upsert':
				/* обычные таски работы с mssql (delins, update, upsert) */
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				if((!Number.isInteger(_task.limit)) || (_task.limit < 1))
					throw new Error('Тип поля limit должен быть integer >= 1.');
				if((!Number.isInteger(_task.cache)) || (_task.cache < 1))
					throw new Error('Тип поля cache должен быть integer >= 1.');
				if((typeof(_task.request) !== 'object') || (Array.isArray(_task.request)))
					throw new Error('Тип поля request должен быть object.');
				if((typeof(_task.table) !== 'object') || (Array.isArray(_task.table))){
					throw new Error('Тип поля table должен быть object.');
				} else if((typeof(_task.table.name) !== 'string') || (_task.table.name === '')){
					throw new Error('Тип поля table.name должен быть string.');
				} else if((typeof(_task.table.margins) !== 'object') || (Array.isArray(_task.table.margins))){
					throw new Error('Тип поля table.margins должен быть object.');
				} else if(Object.keys(_task.table.margins).length < 1){
					throw new Error('Тип поля table.margins должен быть не пустым object.');
				} else if(!Array.isArray(_task.table.matching)){
					throw new Error('Тип поля table.matching должен быть array.');
				} else if(_task.table.matching.length < 1){
					throw new Error('Тип поля table.matching не должен быть пустым массивом.');
				} else if((typeof(_task.table.array) !== 'string') && (typeof(_task.table.array) !== 'undefined')){
					throw new Error('Тип поля table.array должен быть string.');
				} else if((typeof(_task.table.multiline) !== 'string') && (typeof(_task.table.multiline) !== 'undefined')){
					throw new Error('Тип поля table.multiline должен быть string.');
				} else if((!Array.isArray(_task.table.notnull)) && (typeof(_task.table.notnull) !== 'undefined')){
					throw new Error('Тип поля table.notnull должен быть array.');
				} else if((typeof(_task.table.notnull) !== 'undefined') && (_task.table.notnull.length < 1)){
					throw new Error('Тип поля table.notnull не должен быть пустым массивом.');
				} else{
					for(const key in _task.table.matching){
						if(typeof(_task.table.matching[key]) !== 'string')
							throw new Error('Тип поля table.matching[] должен быть string.');
					}
					for(const key in _task.table.margins){
						if(typeof(_task.table.margins[key]) === 'string'){	//простой тип, не проверяю
						} else if(typeof(_task.table.margins[key]) === 'number') {	//простой тип, не проверяю
						} else if(typeof(_task.table.margins[key]) === 'boolean') {	//простой тип, не проверяю
						} else if((typeof(_task.table.margins[key]) === 'object') && (!Array.isArray(_task.table.margins[key]))){
							for(const key2 in _task.table.margins[key]){
								if((key2 !== 'param') && (key2 !== 'requisite') && (key2 !== 'array') && (key2 !== 'multiline') && (key2 !== 'file')){
									throw new Error('Ключ поля table.margins[][] должен быть param, requisite, array, multiline, file.');
								} else {
									if(key2 === 'file'){
										if((typeof(_task.table.margins[key][key2]) !== 'object') || Array.isArray(_task.table.margins[key][key2])){
											throw new Error('Тип поля table.margins[].file должен быть object.');
										} else {
											let filename = Object.keys(_task.table.margins[key][key2])[0];
											let filereq = _task.table.margins[key][key2][filename];
											if((typeof(filename) !== 'string') || (typeof(filereq) !== 'string')){
												throw new Error('Тип поля table.margins[].file должен быть object вида {files[..].name:files[..].requisites[..].id}.');
											}
										}
									}else if(typeof(_task.table.margins[key][key2]) !== 'string'){
										throw new Error('Тип поля table.margins[][] должен быть string.');
									}
									if((key2 === 'array') && ((typeof(_task.table.array) === 'undefined') || (_task.table.array !== _task.table.margins[key][key2]))){
										throw new Error('Имя поля table.margins[][].array не соответствует значению заданному в table.array.');
									}
								}
							}
						} else {
							throw new Error('Тип поля table.margins[] должен быть string, number, boolean или object.');
						}
					}
				}
				break;
			case 'synchronization':
				/* таск работы с mssql (synchronization) */	
				if((!Number.isInteger(_task.limit)) || (_task.limit < 1))
					throw new Error('Тип поля limit должен быть integer >= 1.');
				if((!Number.isInteger(_task.cache)) || (_task.cache < 1))
					throw new Error('Тип поля cache должен быть integer >= 1.');
				if((typeof(_task.request) !== 'object') || (Array.isArray(_task.request)))
					throw new Error('Тип поля request должен быть object.');
				if((typeof(_task.table) !== 'object') || (Array.isArray(_task.table))){
					throw new Error('Тип поля table должен быть object.');
				} else if((typeof(_task.table.name) !== 'string') || (_task.table.name === '')){
					throw new Error('Тип поля table.name должен быть string.');
				} else if((typeof(_task.table.margins) !== 'object') || (Array.isArray(_task.table.margins))){
					throw new Error('Тип поля table.margins должен быть object.');
				} else if(Object.keys(_task.table.margins).length < 1){
					throw new Error('Тип поля table.margins должен быть не пустым object.');
				} else if((typeof(_task.table.array) !== 'string') && (typeof(_task.table.array) !== 'undefined')){
					throw new Error('Тип поля table.array должен быть string.');
				} else if((typeof(_task.table.multiline) !== 'string') && (typeof(_task.table.multiline) !== 'undefined')){
					throw new Error('Тип поля table.multiline должен быть string.');
				} else if((!Array.isArray(_task.table.notnull)) && (typeof(_task.table.notnull) !== 'undefined')){
					throw new Error('Тип поля table.notnull должен быть array.');
				} else if((typeof(_task.table.notnull) !== 'undefined') && (_task.table.notnull.length < 1)){
					throw new Error('Тип поля table.notnull не должен быть пустым массивом.');
				} else{
					for(const key in _task.table.margins){
						if(typeof(_task.table.margins[key]) === 'string'){	//простой тип, не проверяю
						} else if(typeof(_task.table.margins[key]) === 'number') {	//простой тип, не проверяю
						} else if(typeof(_task.table.margins[key]) === 'boolean') {	//простой тип, не проверяю
						} else if((typeof(_task.table.margins[key]) === 'object') && (!Array.isArray(_task.table.margins[key]))){
							for(const key2 in _task.table.margins[key]){
								if((key2 !== 'param') && (key2 !== 'requisite') && (key2 !== 'array') && (key2 !== 'multiline')){
									throw new Error('Ключ поля table.margins[][] должен быть param, requisite, array, multiline.');
								} else {
									if(typeof(_task.table.margins[key][key2]) !== 'string'){
										throw new Error('Тип поля table.margins[][] должен быть string.');
									}
									if((key2 === 'array') && ((typeof(_task.table.array) === 'undefined') || (_task.table.array !== _task.table.margins[key][key2]))){
										throw new Error('Тип поля table.margins[][].array не соответствует значению заданному в table.array.');
									}
								}
							}
						} else {
							throw new Error('Тип поля table.margins[] должен быть string, number, boolean или object.');
						}
					}
				}
				break;
			case 'transaction':
				/* таск работы с mssql (transaction) */	
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				if((!Number.isInteger(_task.limit)) || (_task.limit < 1))
					throw new Error('Тип поля limit должен быть integer >= 1.');
				if((!Number.isInteger(_task.cache)) || (_task.cache < 1))
					throw new Error('Тип поля cache должен быть integer >= 1.');
				if((typeof(_task.request) !== 'object') || (Array.isArray(_task.request)))
					throw new Error('Тип поля request должен быть object.');
				if(!Array.isArray(_task.tables)){
					throw new Error('Тип поля tables должен быть array.');
				} else if(_task.tables.length < 1){
					throw new Error('Тип поля tables должен быть не пустым массивом.');
				} else {
					for(const table in _task.tables){
						if((_task.tables[table].task !== 'insert') && (_task.tables[table].task !== 'update') && (_task.tables[table].task !== 'delete') && (_task.tables[table].task !== 'upsert')){
							throw new Error('Тип поля tables[].task должен быть insert, update, delete, upsert.');
						} else if((typeof(_task.tables[table].name) !== 'string') || (_task.tables[table].name === '')){
							throw new Error('Тип поля tables[].name должен быть string.');
						} else if((typeof(_task.tables[table].margins) !== 'object') || (Array.isArray(_task.tables[table].margins))){
							throw new Error('Тип поля tables[].margins должен быть object.');
						} else if(Object.keys(_task.tables[table].margins).length < 1){
							throw new Error('Тип поля tables[].margins должен быть не пустым object.');
						} else if(!Array.isArray(_task.tables[table].matching)){
							throw new Error('Тип поля tables[].matching должен быть array.');
						} else if(_task.tables[table].matching.length < 1){
							throw new Error('Тип поля tables[].matching не должен быть пустым массивом.');
						} else if((typeof(_task.tables[table].array) !== 'string') && (typeof(_task.tables[table].array) !== 'undefined')){
							throw new Error('Тип поля tables[].array должен быть string.');
						} else if((typeof(_task.tables[table].multiline) !== 'string') && (typeof(_task.tables[table].multiline) !== 'undefined')){
							throw new Error('Тип поля tables[].multiline должен быть string.');
						} else if((!Array.isArray(_task.tables[table].notnull)) && (typeof(_task.tables[table].notnull) !== 'undefined')){
							throw new Error('Тип поля tables[].notnull должен быть array.');
						} else if((typeof(_task.tables[table].notnull) !== 'undefined') && (_task.tables[table].notnull.length < 1)){
							throw new Error('Тип поля tables[].notnull не должен быть пустым массивом.');
						} else{
							for(const key in _task.tables[table].matching){
								if(typeof(_task.tables[table].matching[key]) !== 'string')
									throw new Error('Тип поля tables[].matching[] должен быть string.');
							}
							for(const key in _task.tables[table].margins){
								if(typeof(_task.tables[table].margins[key]) === 'string'){	//простой тип, не проверяю
								} else if(typeof(_task.tables[table].margins[key]) === 'number') {	//простой тип, не проверяю
								} else if(typeof(_task.tables[table].margins[key]) === 'boolean') {	//простой тип, не проверяю
								} else if((typeof(_task.tables[table].margins[key]) === 'object') && (!Array.isArray(_task.tables[table].margins[key]))){
									for(const key2 in _task.tables[table].margins[key]){
										if((key2 !== 'param') && (key2 !== 'requisite') && (key2 !== 'array') && (key2 !== 'multiline')){
											throw new Error('Ключ поля tables[].margins[][] должен быть param, requisite, array, multiline.');
										} else {
											if(typeof(_task.tables[table].margins[key][key2]) !== 'string'){
												throw new Error('Тип поля tables[].margins[][] должен быть string.');
											}
											if((key2 === 'array') && ((typeof(_task.tables[table].array) === 'undefined') || (_task.tables[table].array !== _task.tables[table].margins[key][key2]))){
												throw new Error('Тип поля tables[].margins[][].array не соответствует значению заданному в tables[].array.');
											}
										}
									}
								} else {
									throw new Error('Тип поля tables[].margins[] должен быть string, number, boolean или object.');
								}
							}
						}
					}
				}
				break;
			case 'syncdata':
				/* таски синхронизации данных между api-серверами (syncdata) */
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				if((!Number.isInteger(_task.limit)) || (_task.limit < 1))
					throw new Error('Тип поля limit должен быть integer >= 1.');
				if((!Number.isInteger(_task.cache)) || (_task.cache < 1))
					throw new Error('Тип поля cache должен быть integer >= 1.');
				if((typeof(_task.request) !== 'object') || (Array.isArray(_task.request)))
					throw new Error('Тип поля request должен быть object.');
				if((typeof(_task.sync) !== 'object') || (Array.isArray(_task.sync))){
					throw new Error('Тип поля sync должен быть object.');
				} else if((typeof(_task.sync.remote) !== 'string') || (_task.sync.remote === '')){
					throw new Error('Тип поля sync.remote должен быть string.');
				} else if(["to", "from"].indexOf(_task.sync.type) === -1){
					throw new Error('Тип поля sync.type должен быть to или from.');
				}
				break;
			case 'dbversion':
				/* таски обновления версии СУБД */
				if((typeof(_task.version) !== 'string') || (_task.version === ''))
					throw new Error('Тип поля version должен быть string.');
				if((typeof(_task.dir) !== 'string') || (_task.dir === ''))
					throw new Error('Тип поля dir должен быть string.');
				break;
			default:
				break;
		}
		return true;
	} catch (err){
		return err.message;
	}
}

module.exports.prestart = PreStart;
module.exports.hasher = Hasher;
module.exports.randomgen = RandomGen;
module.exports.uid = generateUID;
module.exports.correcterstr = correcterString;
module.exports.rest = RestRequest;
module.exports.tdata = transformData;
module.exports.regexpstr = RegExpString;
module.exports.toJSONn = DateToJSON;
module.exports.taskvalidator = taskValidator;
module.exports.strDecoder = strToUtf8;
module.exports.comparevers = compareVers;