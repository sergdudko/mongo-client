/**
 *	iRetailCloudClient
 *	(c) 2018 by Siarhei Dudko.
 *
 *	CONSOLE
 *	Консоль клиента (включая установку службы)
 *
 */
 
 "use strict"

if(parseInt(process.version.split(".")[0].substr(1,2), 10) < 10){
	Promise = require("bluebird");
	require("console.table");
	console.tablecustom = console.table;
	console.table = function(a){ console.tablecustom(a); };
}

//подгружаемые библиотеки
var READLINE = require('readline'),
	PATH = require('path'),
	OS = require('os'),
	LODASH = require('lodash'),
	FS = require('fs');
	
	
var NW;
if(OS.platform() === 'win32'){	
	NW = require('node-windows');
} else {
	NW = {
		isAdminUser: function(callback){ callback(true); }
	};
}
	
//подгружаемые модули	
var SERVICE = require(PATH.join(__dirname, 'settings.service.js')),
	SERVICEREALNAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicerealname,
	SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;

function help(arr){
	console.table(arr, ['команда', 'значение']);
}	
function dateFromSilent(_arg){
	let _d = new Date();
	let _start = _arg.split(":");
	let _datestart;
	let _hours = 0, _minutes = 0, _seconds = 0;
	switch(_start.length){
		case 1:		//заданы только часы
			_hours = parseInt(_start[0], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		case 2:		//заданы часы:минуты
			_hours = parseInt(_start[0], 10);
			_minutes = parseInt(_start[1], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		case 3:		//заданы часы:минуты:секунды
			_hours = parseInt(_start[0], 10);
			_minutes = parseInt(_start[1], 10);
			_seconds = parseInt(_start[2], 10);
			_datestart = new Date(_d.getFullYear(), _d.getMonth(), _d.getDate(), _hours, _minutes, _seconds);
			break;
		default:
			throw new Error('Не корректный формат silent!');
	}
	if(typeof(_datestart) !== 'object')
		throw new Error('Не корректный формат silent!');
	else
		return _datestart;
}
NW.isAdminUser(function(isAdmin){
	if (isAdmin) {
		
		let stdout = console;
		
		SERVICE.on('install',function(){
			if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
				stdout.log('Установка службы '+SERVICENAME+' выполнена.');
			}
		});
		SERVICE.on('uninstall',function(){
			if(((!SERVICE.exists) && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && (!SERVICE.exists()) && (OS.platform() === 'linux'))){
				stdout.log('Удаление службы '+SERVICENAME+' выполнено.');
			}
		});
		SERVICE.on('start',function(){
			stdout.log('Запуск службы '+SERVICENAME+' выполнен.');
		});
		SERVICE.on('stop',function(){
			stdout.log('Остановка службы '+SERVICENAME+' выполнена.');
		});
		
		let commands = {
			"--help": {
				title: "Вызов текущей справки.",
				exec: function(){ help(arr); }
			},
			"--install": {
				title: "Установка службы "+SERVICENAME+".",
				exec: function(){
					if(((!SERVICE.exists) && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && (!SERVICE.exists()) && (OS.platform() === 'linux'))){
						SERVICE.install();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Служба "+SERVICENAME+" уже существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--remove": {
				title: "Удаление службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
						SERVICE.uninstall();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--restart": {
				title: "Перезапуск службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
					//новый вариант (с нормальной поддержкой windows)
						if(OS.platform() === 'win32'){
							NW.elevate('TASKKILL /FI "SERVICES EQ '+SERVICEREALNAME+'" /T /F&&EXIT 0');
							setTimeout(stdout.log, 1000, 'Остановка службы '+SERVICENAME+' выполнена.');
						} else {
							SERVICE.stop();
						}
						setTimeout(function(){
							SERVICE.start();
						}, 5000);
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--start": {
				title: "Запуск службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
						SERVICE.start();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--stop": {
				title: "Остановка службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
					//новый вариант (с нормальной поддержкой windows)
						if(OS.platform() === 'win32'){
							NW.elevate('TASKKILL /FI "SERVICES EQ '+SERVICEREALNAME+'" /T /F');
							setTimeout(stdout.log, 1000, 'Остановка службы '+SERVICENAME+' выполнена.');
						} else {
							SERVICE.stop();
						}
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			}
		};
		
		let arr = [];
		for(const key in commands){
			arr.push({"команда":key, "значение":commands[key].title});
		}
		
		let flag = false;
		for(let i =0; i<process.argv.length; i++){
			if(typeof(commands[process.argv[i]]) !== 'undefined'){
				commands[process.argv[i]].exec();
				flag = true;
			}
		}
		if(!flag){
			//ловим эксепшны
			var BUGTRACKER = require(PATH.join(__dirname, 'module.bugtracker.js'));	
			BUGTRACKER(); 
			
			//подгружаемые модули	
			var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
				FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
				PROCSTORE = require(PATH.join(__dirname, 'procstore.js'));
			//разрыв IPC, уничтожаю процесс для определенности состояния
			PROCSTORE.stderr = function(err){
				if(err.indexOf('This socket has been ended by the other party') !== -1){
					LOGGER.warn('IPC ReduxCluster write error');
					setTimeout(function(){
						process.exit(1); 
					}, 5000);
				}
			}
			PROCSTORE.createClient({path:PATH.join(__dirname, SERVICENAME+".sock"), login:"iretail", password:"santaclaus"});
			
			stdout = LOGGER;
			//добавляю команды, недоступные без оболочки (требуется подключение к основному процессу)
			let _commands = { 
				"--tasklog": {
					title: "Вывести информацию о последнем успешном выполнении заданий.",
					exec: function(line){
						function algorithm(data){
							if((typeof(data) === 'string') || (typeof(data.toString()) === 'string')){
								let _data = data.toString().toLowerCase().replace(/ {2}/gi, " ").replace(/ {2}/gi, " ").replace(/ {2}/gi, " ").replace(/ {2}/gi, " ").replace(/ {2}/gi, " ").split(" ").map(function(_d){ 	//срез до 25 пробелов -> массив
									if(_d.length > 10){
										_d = _d.substr(0, _d.length - 3);
									} else if (_d.length > 5){
										_d = _d.substr(0, _d.length - 2)
									} else if (_d.length > 3){
										_d = _d.substr(0, _d.length - 1);
									}
									return _d;
								});
								return _data;
							} else {
								return new Array();
							}
						}
						let _line;
						if(line){
							_line = algorithm(line.replace("--tasklog ", ""));
						}
						function testText(val){
							if(!_line){
								return true;
							} else {
								let _val = algorithm(val);
								for(let id = 0; id < _line.length; id++){
									if(_val.indexOf(_line[id]) !== -1) { return true; }	//быстрый поиск
									for(let id2 = 0; id2 < _val.length; id2++){	//медленный поиск
										if(_val[id2].indexOf(_line[id]) !== -1){
											return true;
										}
									}
									if(id === (_line.length -1))
										return false;
								}
							}
						}
						if(PROCSTORE.connected){
							let _tmp = [];
							for(const key in PROCSTORE.getState().names){
								if(typeof(PROCSTORE.getState().logs[key]) === 'undefined'){
									if(PROCSTORE.getState().run[key]){
										if(testText(PROCSTORE.getState().names[key]) || testText(PROCSTORE.getState().tasks[key].description) || testText(PROCSTORE.getState().tasks[key].author)){
											_tmp.push({
												'задача': PROCSTORE.getState().names[key],
												'описание': PROCSTORE.getState().tasks[key].description,
												'автор': PROCSTORE.getState().tasks[key].author,
												'выполнена': 'не выполнялось',
												'следующий запуск': 'выполняется '+(parseInt((Date.now()-PROCSTORE.getState().run[key].timestamp)/1000))+'s'
											});
										}
									}else{
										if(testText(PROCSTORE.getState().names[key]) || testText(PROCSTORE.getState().tasks[key].description) || testText(PROCSTORE.getState().tasks[key].author)){
											_tmp.push({
												'задача': PROCSTORE.getState().names[key],
												'описание': PROCSTORE.getState().tasks[key].description,
												'автор': PROCSTORE.getState().tasks[key].author,
												'выполнена': 'не выполнялось',
												'следующий запуск': (function(){
													let _d = Date.now();
													let _next = _d;
													let _silent = PROCSTORE.getState().tasks[key].silent;
													if(Array.isArray(_silent)){
														for(let _i = 0; _i < _silent.length; _i++){
															if((_next >= _silent[_i].start) && (_next <= _silent[_i].end)){
																_next = _silent[_i].end+1;	//запуск это время окончания silent со сдвигом в 1 мс
															}
														}
													}
													if(_d !== _next){
														return 'в очередь через '+parseInt(((_next - _d)/1000), 10)+'s';
													} else {
														return 'в очереди';
													}
												})()
											});
										}
									}
								} else {
									let _d = PROCSTORE.getState().logs[key]+(PROCSTORE.getState().tasks[key].interval*60000);
									let _next = _d;
									let _silent = PROCSTORE.getState().tasks[key].silent;
									let _ttime = parseInt(((_d - Date.now())/1000), 10);
									if(Array.isArray(_silent)){
										for(let _i = 0; _i < _silent.length; _i++){
											if((_next >= _silent[_i].start) && (_next <= _silent[_i].end)){
												_next = _silent[_i].end+1;	//запуск это время окончания silent со сдвигом в 1 мс
											}
										}
									}
									if(_d !== _next){
										_ttime = parseInt(((_next - Date.now())/1000), 10);
									}
									if(testText(PROCSTORE.getState().names[key]) || testText(PROCSTORE.getState().tasks[key].description) || testText(PROCSTORE.getState().tasks[key].author)){
										_tmp.push({
											'задача': PROCSTORE.getState().names[key],
											'описание': PROCSTORE.getState().tasks[key].description,
											'автор': PROCSTORE.getState().tasks[key].author,
											'выполнена': (new Date(PROCSTORE.getState().logs[key])).toLocaleString(),
											'следующий запуск': (function(){
												if(_ttime > 0){
													return 'в очередь через '+_ttime+'s';
												} else {
													if(PROCSTORE.getState().run[key]){
														return 'выполняется '+(parseInt((Date.now()-PROCSTORE.getState().run[key].timestamp)/1000))+'s';
													}else{
														return 'в очереди';
												}}})()
										});
									}
								}
							}
							const _result = LODASH.sortBy(_tmp, ['выполнена', 'задача']);
							console.table(_result, ['задача', 'описание', 'автор', 'выполнена', 'следующий запуск']);
						} else {
							LOGGER.error("Сокет не подключен, проверьте состояние службы "+SERVICENAME+"!");
						}
					}
				},
				"--clearlog":{
					title: "Очистить лог заданий (очередь будет перезапущена).",
					exec: function(){
						PROCSTORE.dispatch({type:'CLEAR_LOG'});
						LOGGER.log("Лог задач очищен!");
					}
				},
				"--cleartask":{
					title: "Очистить лог задания (будет поставлено в очередь). Выполнять как --cleartask [TASK]",
					exec: function(_line){
						if(PROCSTORE.connected){
							if(_line){
								let _linearr = _line.split(" ");
								let _flg = false;
								let _arr = [];
								for(const uid in PROCSTORE.getState().names){
									let _taskname = PROCSTORE.getState().names[uid];
									if((_linearr.indexOf(_taskname) !== -1) || (_linearr.indexOf(_taskname.substr(0,_taskname.length-5)) !== -1)){
										PROCSTORE.dispatch({type:'CLEAR_LOG_TASK', payload:uid});
										_flg = true;
										_arr.push(_taskname);
									}
								}
								if(_flg){
									LOGGER.log("Лог задач "+_arr.join(",")+" очищен!");
								} else {
									LOGGER.error("Соответствующие задачи не найдены!");
								}
							} else {
								LOGGER.error("Аргумент наименование_задачи не задан!");
							}
						} else {
							LOGGER.error("Сокет не подключен, проверьте состояние службы "+SERVICENAME+"!");
						}
					}
				},
				"--taskread":{
					title: "Перечитать все задачи из папки.",
					exec: function(){
						if(PROCSTORE.connected){
							FS.readdir(PATH.join(__dirname, '/tasks/'), function(err,filesall){
								try{
									if(err){
										throw err;
									} else {
										let files = [];
										for(let key = 0; key < filesall.length; key++){ 
											if(filesall[key].substr(-5).toString() === '.json'){	//выбираем только файлы .json
												files.push(filesall[key]);
											}
										}
										let tasks = {}, names = {};
										let flag = 0;
										for(let key = 0; key < files.length; key++){
											let filename = files[key];
											FS.readFile(PATH.join(__dirname, '/tasks/', filename), "utf8", (err, data) => {
												try{
													let _data = "";
													if (err) {
														throw err;
													} else {
														const _tmp = data.split("***");
														_data = _tmp[0];	//первый элемент выборки
														for(let i = 1; (i+1) < _tmp.length; i=i+2){
															const _var = PROCSTORE.getState().variable[_tmp[i]]; //всегда первый элемент выборки
															if(typeof(_var) !== 'undefined'){
																if(_data[_data.length-1] !== '"')
																	_data = _data + '"';
																_data = _data + _var;
																if(_tmp[i+1][0] !== '"')	//всегда второй элемент выборки
																	_data = _data + '"';
																_data = _data + _tmp[i+1];
															}else 
																throw new Error("Неизвестная переменная "+_tmp[i]+"!");
														}
														const _object = JSON.parse(_data);
														
														if(_object.silent){
															let _silent = _object.silent;
															let _silentarr = _silent.split(';');
															for(let _i = 0; _i < _silentarr.length; _i++){
																if(_silentarr[_i] !== ""){
																	let _silentinterval = _silentarr[_i].split('-');
																	if(_silentinterval.length !== 2){
																		throw new Error('Не корректный диапазон silent, формат должен быть 01:05-10:00;11:00-12:00; !');
																	} else{
																		let _tmpinterval = {
																			start: dateFromSilent(_silentinterval[0]).getTime(), 
																			end: dateFromSilent(_silentinterval[1]).getTime()
																		};
																		if(_tmpinterval.start > _tmpinterval.end){
																			throw new Error('Не корректный формат silent, время начала не может быть больше времени окончания!');
																		}
																		if(!Array.isArray(_object.silent)){
																			_object.silent = [_tmpinterval];
																		} else{
																			_object.silent.push(_tmpinterval);
																		}
																	}
																}
															}
															if(Array.isArray(_object.silent)){
																_object.silent = LODASH.sortBy(_object.silent, 'start');
															}
														}														
														let _valid = FUNCTIONS.taskvalidator(_object);
														if(_valid === true){
															tasks[FUNCTIONS.hasher(filename)] = _object; 
															names[FUNCTIONS.hasher(filename)] = filename;
														} else {
															throw new Error(_valid);
														}
													}
												} catch(err){
													LOGGER.error("CONSOLE-> Ошибка при чтении таска "+filename+":" + err);
												}
												flag++;
												if(flag === (files.length)){
													PROCSTORE.dispatch({type:'UPDATE_TASKS', payload: {"tasks":tasks, "names":names}});
													LOGGER.log("CONSOLE-> Задачи прочитаны!");
												}
											});
										}
									}
								} catch(err){
									LOGGER.error("CONSOLE-> Ошибка при чтении каталога с тасками:" + err);
								}
							});		
						} else {
							LOGGER.error("Сокет не подключен, проверьте состояние службы "+SERVICENAME+"!");
						}
					}
				}
			};
			
			for(const key in _commands){
				commands[key] = _commands[key];
				arr.push({"команда":key, "значение":commands[key].title});
			}
			
			help(arr);
			const rl = READLINE.createInterface({
				input: process.stdin,
				output: process.stdout,
				prompt: SERVICENAME+'> '
			});	
			rl.prompt();
			rl.on('line', (line) => {
				try{
					if(typeof(commands[line]) !== 'undefined'){
						commands[line].exec();
					} else if(line.indexOf("--cleartask ") !== -1){
						commands["--cleartask"].exec(line);
					} else if(line.indexOf("--tasklog ") !== -1){
						commands["--tasklog"].exec(line);
					} else {
						let _tmp = eval(line);
						if(typeof(_tmp) !== 'undefined'){
							console.log(_tmp);
						}
					}
				} catch(err) {
					stdout.error("Ошибка команды: "+err);
					help(arr);
				}
				rl.prompt();
			}).on('close', () => {
				stdout.log(SERVICENAME+' disconnected!');
				process.exit(0);
			});
		}
		
	} else {
		console.error('Запуск требует административных привелегий.');
	}
});