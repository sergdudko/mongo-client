/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  BUGTRACKER
 *	Запись исключительных ситуаций
 *
 */

"use strict"

//подгружаемые библиотеки
var FS = require('fs'),
	PATH = require('path');
	
//подгружаемые модули
var SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;

function bugtracker(){
	let _filepath = PATH.join("logs", SERVICENAME+'-exception.log');
	process.on('uncaughtException', (err) => {
		let _str = '\n'+new Date().toJSON()+'\n'+err.message + '\n' + err.stack;
		FS.writeFileSync(_filepath, _str, {flag: "a"});
		process.exit(1);
	});
	process.on('unhandledRejection', (reason, p) => {
		let _str = '\n'+new Date().toJSON()+'\n'+'Unhandled Rejection at:'+p+'reason:'+reason;
		FS.writeFileSync(_filepath, _str, {flag: "a"});
		process.exit(1);
	});
}
	
module.exports = bugtracker;